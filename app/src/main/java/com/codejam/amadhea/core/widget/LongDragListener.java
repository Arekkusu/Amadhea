package com.codejam.amadhea.core.widget;

import android.content.ClipData;
import android.view.View;

import com.codejam.amadhea.core.intefaze.IClickListener;

/**
 * This file was created by Snack on 08/03/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

/**
 * Clase que arrastra un {@link View} cuando el usuario mantiene click sobre este.
 */
public class LongDragListener implements View.OnLongClickListener {

    private IClickListener dragListener;

    /**
     * Constructor default.
     */
    public LongDragListener() {}

    /**
     * Constructor que acepta un listener de la interfaz {@link IClickListener}.
     * <p>
     * Cuando un usuario matiene click sobre un view con este listener,
     * el {@link IClickListener} pasado por el constructor es activado con
     * {@code  listener.onClick(view);}
     * </p>
     *
     * @param dragListener El listener
     */
    public LongDragListener(IClickListener dragListener) {
        this.dragListener = dragListener;
    }

    @SuppressWarnings("unchecked, deprecation")
    @Override
    public boolean onLongClick(View view) {
        ClipData data = ClipData.newPlainText("", "");
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
        view.startDrag(data, shadowBuilder, view, 0);
        if (dragListener != null)
            dragListener.onClick(view);
        return true;
    }
}
