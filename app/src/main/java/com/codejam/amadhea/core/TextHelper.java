package com.codejam.amadhea.core;

import android.support.test.espresso.core.deps.guava.collect.Lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This file was created by Snack on 30/01/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

@SuppressWarnings("unused")
public final class TextHelper {

    private TextHelper() {}

    /**
     * Concatena un array de strings.
     *
     * @param strings Un array de strings
     * @return Un gran {@link String}
     */
    public static String concatStrings(String... strings) {
        String append = "";
        for (String string : strings) {
            append += string;
        }
        return append;
    }

    /**
     * Concatena un array de strings con un caracter como separador.
     *
     * @param separator Separador
     * @param strings Un array de strings
     * @return Un gran {@link String}
     */
    public static String concatStrings(char separator, String... strings) {
        String append = "";
        for (int i = 0; i < strings.length - 1; i++) {
            append += strings[0] + separator;
        }
        return append + strings[strings.length - 1];
    }

    /**
     * Concatena un array de strings con un separador.
     *
     * @param separator Separador
     * @param strings Un array de strings
     * @return Un gran {@link String}
     */
    public static String concatStrings(String separator, String... strings) {
        String append = "";
        for (int i = 0; i < strings.length - 1; i++) {
            append += strings[0] + separator;
        }
        return append + strings[strings.length - 1];
    }

    /**
     * Convierte a lista un array de strings.
     *
     * @param args Un array de {@link T}
     * @return Una lista de {@link T}
     */
    public static <T> List<T> toList(T[] args) {
        return Lists.newArrayList(args);
    }

    /**
     * Convierte a lista un array de array de strings.
     *
     * @param args Un array de array de {@link T}
     * @return Una lista de {@link T}
     */
    public static <T> List<T> toListMixed(T[][] args) {
        ArrayList<T> list = new ArrayList<>();
        for (T[] arg : args) {
            Collections.addAll(list, arg);
        }
        return list;
    }

    /**
     * Convierte a lista de arrays un array de array de strings.
     *
     * @param args Un array de array de {@link T}
     * @return Una lista de arrays de {@link T}
     */
    public static <T> List<T[]> toListSeparate(T[][] args) {
        ArrayList<T[]> listSeparate = new ArrayList<>();
        Collections.addAll(listSeparate, args);
        return listSeparate;
    }
}
