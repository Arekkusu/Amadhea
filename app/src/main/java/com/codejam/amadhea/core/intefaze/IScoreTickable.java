package com.codejam.amadhea.core.intefaze;

import android.view.View;

/**
 * This file was created by Snack on 28/02/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

/**
 * Interfaz utilizada para actividades de juegos que tienen un {@link View} que deseen vigilar
 *
 * @param <T> El {@link View} que activa el listener
 */
public interface IScoreTickable<T extends View> {

    /**
     * Inicializa el listener.
     *
     * @param t El {@link View} especificado
     */
    void initListener(T t);

    /**
     * Cuando el view hace un update.
     *
     * @param t El {@link View} especificado
     */
    void onTick(T t);
}
