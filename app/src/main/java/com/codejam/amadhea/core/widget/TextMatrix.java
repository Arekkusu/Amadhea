package com.codejam.amadhea.core.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * This file was created by Snack on 04/04/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public class TextMatrix extends TextView {

    private int[][] neoMatrix;

    public TextMatrix(Context context) {
        super(context);
    }

    public TextMatrix(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextMatrix(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public int[][] getNeoMatrix() {
        return neoMatrix;
    }

    public void setNeoMatrix(int[][] neoMatrix) {
        this.neoMatrix = neoMatrix;

        String chars = "";
        for (int i = 0, neoMatrixLength = neoMatrix.length; i < neoMatrixLength; i++) {
            int[] row = neoMatrix[i];
            for (int j = 0, rowLength = row.length; j < rowLength; j++) {
                int c = row[j];
                chars += c;
                if(j != rowLength - 1) {
                    chars += " ";
                }
            }
            if(i != neoMatrixLength - 1) {
                chars += "\n";
            }
        }
        setText(chars);
    }
}
