package com.codejam.amadhea.core.widget;

import android.view.DragEvent;
import android.view.View;

import com.codejam.amadhea.core.intefaze.IDropListener;

/**
 * This file was created by Snack on 05/03/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

/**
 * Clase que coloca un {@link View} sobre otro {@link View} cuando el primero es soltado encima del otro
 */
public class DropListener implements View.OnDragListener {

    private IDropListener listener;

    /**
     * Constructor default.
     */
    public DropListener() {}

    /**
     * Constructor que acepta un listener de la interfaz {@link IDropListener}.
     * <p>
     * Cuando un usuario suelta un view sobre otro el {@link IDropListener}
     * es activado con {@code listener.onDrop(dropTarget, dropped);}
     * </p>
     *
     * @param listener El listener
     */
    public DropListener(IDropListener listener) {
        this.listener = listener;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean onDrag(View dropTarget, DragEvent event) {
        switch (event.getAction()) {
            case DragEvent.ACTION_DROP:

                View dropped = (View) event.getLocalState();

                Object tag = dropTarget.getTag();

                if (tag == null) {
                    dropTarget.setTag(dropped.getId());
                    if (listener != null)
                        listener.onDrop(dropTarget, dropped);
                }
                break;
            default:
                break;
        }
        return true;
    }
}
