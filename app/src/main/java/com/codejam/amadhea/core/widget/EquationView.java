package com.codejam.amadhea.core.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.x5.template.Chunk;
import com.x5.template.Theme;
import com.x5.template.providers.AndroidTemplates;

/**
 * This file was created by Snack on 31/01/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public class EquationView extends WebView {

    private String mText;

    @SuppressLint("SetJavaScriptEnabled")
    public EquationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            getSettings().setJavaScriptEnabled(true);
            getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            setBackgroundColor(Color.TRANSPARENT);

            TypedArray mTypeArray = context.getTheme().obtainStyledAttributes(
                    attrs,
                    io.github.kexanie.library.R.styleable.MathView,
                    0, 0
            );

            try {
                setText(mTypeArray.getString(io.github.kexanie.library.R.styleable.MathView_text));
            }
            finally {
                mTypeArray.recycle();
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    private Chunk getChunk() {
        String template = "katex";
        AndroidTemplates loader = new AndroidTemplates(getContext());

        return new Theme(loader).makeChunk(template);
    }

    public EquationView setText(String text) {
        mText = text;

        Chunk chunk = getChunk();
        chunk.set("formula", mText);
        this.loadDataWithBaseURL(null, chunk.toString(), "text/html", "utf-8", "about:blank");
        return this;
    }

    public String getText() {
        return mText;
    }
}
