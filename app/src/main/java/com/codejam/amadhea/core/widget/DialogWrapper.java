package com.codejam.amadhea.core.widget;

import android.app.Dialog;
import android.content.Context;

/**
 * This file was created by Snack on 30/03/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

/**
 * Clase que crea un Dialog de pantalla completa con un layout.
 */
public class DialogWrapper {

    private final Context context;
    private boolean cancelable;
    private int feature;
    private int id;

    /**
     * Crea un nuevo {@link DialogWrapper}.
     *
     * @param context El contexto de la aplicación
     * @param id El id de un layout
     */
    public DialogWrapper(Context context, int id) {
        this.context = context;
        this.id = id;
    }

    /**
     * Añade una característica al Dialog.
     *
     * @param feature Un {@link android.view.Window}
     * @return El DialogWrapper
     */
    public DialogWrapper setFeature(int feature) {
        this.feature = feature;
        return this;
    }

    /**
     * Define si el Dialogo puede ser cancelado.
     *
     * @param cancelable Si es cancelable
     * @return El DialogWrapper
     */
    public DialogWrapper setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
        return this;
    }

    /**
     * Crea el {@link Dialog}.
     *
     * @return Un Dialog
     */
    public Dialog build() {
        Dialog dialog = new Dialog(context);
        if (feature != 0) {
            dialog.requestWindowFeature(feature);
        }
        dialog.setContentView(id);
        dialog.setCancelable(cancelable);
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.90);
        if (dialog.getWindow() != null)
            dialog.getWindow().setLayout(width, height);
        return dialog;
    }
}
