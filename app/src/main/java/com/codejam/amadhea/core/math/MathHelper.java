package com.codejam.amadhea.core.math;

import java.util.Random;

/**
 * This file was created by Snack on 23/01/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

@SuppressWarnings("unused")
public final class MathHelper {

    private MathHelper(){}

    public static int clamp(int num, int min, int max) {
        return num > min ? (num < max ? num : max) : min;
    }

    public static float clamp(float num, float min, float max) {
        return num > min ? (num < max ? num : max) : min;
    }

    public static double clamp(double num, double min, double max) {
        return num > min ? (num < max ? num : max) : min;
    }

    /**
     * Genera un número random entre dos números.
     *
     * @param random Un objeto {@link Random}
     * @param min El numero mínimo obtenible
     * @param max El numero máximo obtenible
     * @return Un número
     */
    public static int randomBetween(Random random, int min, int max) {
        int range = max - min;
        return min + random.nextInt(range);
    }
}
