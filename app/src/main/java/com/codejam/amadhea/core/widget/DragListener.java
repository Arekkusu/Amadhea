package com.codejam.amadhea.core.widget;

import android.content.ClipData;
import android.view.MotionEvent;
import android.view.View;

import com.codejam.amadhea.core.intefaze.IDragListener;

/**
 * This file was created by Snack on 05/03/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

/**
 * Clase que arrastra un {@link View} cuando el usuario hace click sobre este.
 */
public class DragListener implements View.OnTouchListener {

    private IDragListener dragListener;

    /**
     * Constructor default.
     */
    public DragListener() {}

    /**
     * Constructor que acepta un listener de la interfaz {@link IDragListener}.
     * <p>
     * Cuando un usuario hace click sobre un view, el listener {@link IDragListener}
     * es activado con {@code listener.onDrag(view);}
     * </p>
     *
     * @param dragListener El listener
     */
    public DragListener(IDragListener dragListener) {
        this.dragListener = dragListener;
    }

    @SuppressWarnings("unchecked, deprecation")
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            ClipData data = ClipData.newPlainText("", "");
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
            view.startDrag(data, shadowBuilder, view, 0);
            if (dragListener != null)
                dragListener.onDrag(view);
            return true;
        } else {
            return false;
        }
    }
}
