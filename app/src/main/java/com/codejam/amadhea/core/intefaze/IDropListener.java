package com.codejam.amadhea.core.intefaze;

import android.view.View;

/**
 * This file was created by Snack on 05/03/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

/**
 * Interfaz utilizada por {@link com.codejam.amadhea.core.widget.DropListener}
 *
 * @param <T> El {@link View} que activa el listener
 */
public interface IDropListener<T extends View> {

    /**
     * Cuando un {@link View} especificado por el {@link T} es soltado sobre otro view.
     *
     * @param target El {@link View} al que se suelta
     * @param drop El {@link View que se suelta}
     */
    void onDrop(T target, T drop);
}
