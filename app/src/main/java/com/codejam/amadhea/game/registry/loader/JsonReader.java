package com.codejam.amadhea.game.registry.loader;

import android.content.res.Resources;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;

/**
 * This file was created by Snack on 28/01/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */
@SuppressWarnings({"unused","WeakerAccess"})
public class JsonReader {

    private final String LOGTAG = JsonReader.class.getSimpleName();

    /**
     * Lee el archivo con extensión .json y lo almacena en un {@link String}.
     *
     * @param resources Los {@link Resources} de la aplicación
     * @param id        El id del archivo, se consigue con "R.carpeta.archivo"
     * @return El json String
     */
    public String readJSON(Resources resources, int id) {
        InputStream resourceReader = resources.openRawResource(id);
        Writer writer = new StringWriter();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(resourceReader, "UTF-8"));
            String line = reader.readLine();
            while (line != null) {
                writer.write(line);
                line = reader.readLine();
            }
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Unhandled exception while using JsonReader", e);
        }
        finally {
            try {
                resourceReader.close();
            }
            catch (Exception e) {
                Log.e(LOGTAG, "Unhandled exception while using JsonReader", e);
            }
        }

        return writer.toString();
    }
}
