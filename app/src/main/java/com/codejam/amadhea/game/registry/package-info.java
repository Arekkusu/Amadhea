@ParametersAreNonnullByDefault
@MethodsAreNonnullByDefault
package com.codejam.amadhea.game.registry;

import com.codejam.amadhea.core.intefaze.MethodsAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;