package com.codejam.amadhea.game.resource.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This file was created by Snack on 20/02/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

@SuppressWarnings("deprecation, unused")
public final class ProfileHandler {

    @Deprecated
    private static final String AMADHEA_PROFILES = "amadhea_profiles";

    private ProfileHandler() {}

    /**
     * Retorna una lista de los Perfiles registrados en la aplicación.
     *
     * @param context El {@link Context}, usado para acceder a los recursos de la aplicación
     * @return Una lista de {@link Profile}
     */
    public static List<Profile> getProfiles(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(AMADHEA_PROFILES, Context.MODE_PRIVATE);
        List<String> keys = new ArrayList<>(preferences.getAll().keySet());
        List<Profile> profiles = new ArrayList<>(keys.size());

        for (String uuid : keys) {
            profiles.add(new Profile(uuid, preferences.getString(uuid, "")));
        }

        return profiles;
    }

    /**
     * Retorna una lista de los UUID de los Perfiles registrados en la aplicación.
     *
     * @param context El {@link Context}, usado para acceder a los recursos de la aplicación
     * @return Una lista de Strings
     */
    public static List<String> getProfileKeys(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(AMADHEA_PROFILES, Context.MODE_PRIVATE);

        return new ArrayList<>(preferences.getAll().keySet());
    }

    /**
     * Retorna un Perfil del mapa de Perfiles de la aplicación.
     *
     * @param context El {@link Context}, usado para acceder a los recursos de la aplicación
     * @param uuid    El UUID del perfil
     * @return El {@link Profile}, si existe
     */
    @Nullable
    public static Profile getProfile(Context context, String uuid) {
        SharedPreferences preferences = context.getSharedPreferences(AMADHEA_PROFILES, Context.MODE_PRIVATE);
        String name = preferences.getString(uuid, null);

        return name == null ? null : new Profile(uuid, name);
    }

    /**
     * Retorna un nombre de Perfil del mapa de Perfiles de la aplicación.
     *
     * @param context El {@link Context}, usado para acceder a los recursos de la aplicación
     * @param uuid    El UUID del perfil
     * @return El nombre del perfil
     */
    public static String getProfileName(Context context, String uuid) {
        SharedPreferences preferences = context.getSharedPreferences(AMADHEA_PROFILES, Context.MODE_PRIVATE);

        return preferences.getString(uuid, "");
    }

    /**
     * Añade un Perfil al mapa de Perfiles de la aplicación.
     *
     * @param context El {@link Context}, usado para acceder a los recursos de la aplicación
     * @param name    El nombre del nuevo perfil
     */
    public static void addProfile(Context context, String name) {
        SharedPreferences preferences = context.getSharedPreferences(AMADHEA_PROFILES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(UUID.randomUUID().toString(), name);
        editor.apply();
    }

    /**
     * Modifica el nombre de un Perfil del mapa de Perfiles de la aplicación.
     *
     * @param context El {@link Context}, usado para acceder a los recursos de la aplicación
     * @param uuid    El UUID del perfil
     * @param newName El nuevo nombre del perfil
     */
    public static void modifyProfile(Context context, String uuid, String newName) {
        SharedPreferences preferences = context.getSharedPreferences(AMADHEA_PROFILES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(uuid, newName);
        editor.apply();
    }

    /**
     * Remueve un Perfil del mapa de Perfiles de la aplicación.
     *
     * @param context El {@link Context}, usado para acceder a los recursos de la aplicación
     * @param uuid    El UUID del perfil
     */
    public static void removeProfile(Context context, String uuid) {
        SharedPreferences preferences = context.getSharedPreferences(AMADHEA_PROFILES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(uuid);
        editor.apply();
    }
}
