package com.codejam.amadhea.game.registry.data;

import android.support.annotation.NonNull;

/**
 * This file was created by Snack on 28/01/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

@SuppressWarnings({"unused","WeakerAccess"})
public interface IMathData<M,R,T> {

    T getType();

    @Deprecated
    void setType(int type);

    M getString();

    @Deprecated
    void setString(@NonNull M math);

    R getAnswer();

    @Deprecated
    void setAnswer(@NonNull R results);

    boolean isCorrect(R r, T t);
}
