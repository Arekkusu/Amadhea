package com.codejam.amadhea.game.registry;

import com.codejam.amadhea.game.registry.data.Conjuntos;
import com.codejam.amadhea.game.registry.data.Equation;
import com.codejam.amadhea.game.registry.data.Function;
import com.codejam.amadhea.game.registry.data.Matrix;
import com.codejam.amadhea.game.registry.data.Statistic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * This file was created by Snack on 28/01/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

@SuppressWarnings({"WeakerAccess", "unused"})
public final class GameRegistry {

    private static final Map<Equation.Type, ArrayList<Equation>> equationMap = new HashMap<>();
    private static final Map<Function.Type, ArrayList<Function>> functionMap = new HashMap<>();
    private static final List<Statistic> statisticList = new ArrayList<>();
    private static final List<Conjuntos> conjuntosList = new ArrayList<>();
    private static final List<Matrix> matrixList = new ArrayList<>();
    public static final Random RAND = new Random();

    /**
     * Crea en {@link equationMap} y {@link functionMap} los distintos tipos de ecuaciones con sus respectivos {@link ArrayList}.
     */
    static {
        //Ecuaciones
        for (Equation.Type type : Equation.Type.values()) {
            equationMap.put(type, new ArrayList<Equation>());
        }
        //Funciones
        for (Function.Type type : Function.Type.values()) {
            functionMap.put(type, new ArrayList<Function>());
        }
    }

    private GameRegistry() {}

    /**
     * Registra una ecuación según su {@link Equation.Type}
     *
     * @param expression Una {@link Equation}
     */
    public static void register(Equation expression) {
        Equation.Type type = expression.getType();
        equationMap.get(type).add(expression);
    }

    /**
     * Registra una función según su {@link Function.Type}
     *
     * @param function Una {@link Function}
     */
    public static void register(Function function) {
        Function.Type type = function.getType();
        functionMap.get(type).add(function);
    }

    /**
     * Registra una estadística
     *
     * @param statistic Una {@link Statistic}
     */
    public static void register(Statistic statistic) {
        statisticList.add(statistic);
    }

    public static void register(Matrix statistic) {
        matrixList.add(statistic);
    }

    /**
     * Registra un conjunto
     *
     * @param conjuntos Un {@link Conjuntos}
     */
    public static void register(Conjuntos conjuntos){
        conjuntosList.add(conjuntos);
    }

    /**
     * Clase de ayuda para ecuaciones.
     */
    public static final class EQUATION {

        /**
         * Regresa una {@link List} de {@link Equation} según el tipo de ecuación.
         *
         * @param type El {@link Equation.Type}
         * @return Una lista
         */
        public static List<Equation> getList(Equation.Type type) {
            return equationMap.get(type);
        }

        /**
         * Regresa una lista de ecuaciones random según su tipo.
         *
         * @param type El {@link Equation.Type}
         * @param size El tamaño de la lista
         * @return Una lista de {@link Equation}
         */
        public static List<Equation> getRandomList(Equation.Type type, int size) {
            List<Equation> list = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                list.add(equationMap.get(type).get(i));
            }
            Collections.shuffle(list);
            return list;
        }

        /**
         * Regresa una lista de ecuaciones sin tomar en cuenta su tipo.
         *
         * @param size El tamaño de la lista
         * @return Una lista de {@link Equation}
         */
        public static List<Equation> getRandomList(int size) {
            List<Equation> list = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                int num = 1 + RAND.nextInt(Equation.Type.values().length);
                list.add(equationMap.get(Equation.Type.values()[RAND.nextInt(num)]).get(i));
            }
            Collections.shuffle(list);
            return list;
        }

        /**
         * Regresa una ecuación random según su tipo.
         *
         * @param type El {@link Equation.Type}
         * @return Una {@link Equation}
         */
        public static Equation getRandom(Equation.Type type) {
            List<Equation> list = equationMap.get(type);
            return list.get(RAND.nextInt(list.size()));
        }

        /**
         * Regresa una ecuación sin tomar en cuenta su tipo.
         *
         * @return Una {@link Equation}
         */
        public static Equation getRandom() {
            int num = RAND.nextInt(Equation.Type.values().length);
            List<Equation> list = equationMap.get(Equation.Type.values()[num]);
            num = list.size() > 0 ? RAND.nextInt(list.size()) : 0;
            return list.get(num);
        }
    }

    /**
     * Clase de ayuda para funciones.
     */
    public static final class FUNCTION {

        /**
         * Regresa una {@link List} de {@link Function} según el tipo de función.
         *
         * @param type El {@link Function.Type}
         * @return Una lista
         */
        public static List<Function> getList(Function.Type type) {
            return functionMap.get(type);
        }

        /**
         * Regresa una lista de funciones random según su tipo.
         *
         * @param type El {@link Function.Type}
         * @param size El tamaño de la lista
         * @return Una lista de {@link Function}
         */
        public static List<Function> getRandomList(Function.Type type, int size) {
            List<Function> list = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                list.add(functionMap.get(type).get(i));
            }
            Collections.shuffle(list);
            return list;
        }

        /**
         * Regresa una lista de funciones sin tomar en cuenta su tipo.
         *
         * @param size El tamaño de la lista
         * @return Una lista de {@link Function}
         */
        public static List<Function> getRandomList(int size) {
            List<Function> list = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                int num = RAND.nextInt(Function.Type.values().length);
                list.add(functionMap.get(Function.Type.values()[RAND.nextInt(num)]).get(i));
            }
            Collections.shuffle(list);
            return list;
        }

        /**
         * Regresa una función random según su tipo.
         *
         * @param type El {@link Function.Type}
         * @return Una {@link Function}
         */
        public static Function getRandom(Function.Type type) {
            List<Function> list = functionMap.get(type);
            return list.get(RAND.nextInt(list.size()));
        }

        /**
         * Regresa una función sin tomar en cuenta su tipo.
         *
         * @return Una {@link Function}
         */
        public static Function getRandom() {
            int num = RAND.nextInt(Function.Type.values().length);
            List<Function> list = functionMap.get(Function.Type.values()[num]);
            num = list.size() > 0 ? RAND.nextInt(list.size()) : 0;
            return list.get(num);
        }
    }

    /**
     * Clase de ayuda para estadística.
     */
    public static final class STATISTIC {

        /**
         * Regresa una lista de estatísticas.
         *
         * @return Una {@link Statistic}
         */
        public static List<Statistic> getList() {
            return statisticList;
        }

        /**
         * Regresa una lista de estatísticas.
         *
         * @param size El tamaño de la lista
         * @return Una lista de {@link Statistic}
         */
        public static List<Statistic> getRandomList(int size) {
            List<Statistic> list = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                list.add(statisticList.get(i));
            }
            Collections.shuffle(list);
            return list;
        }

        /**
         * Regresa una estatística.
         *
         * @return Una {@link Statistic}
         */
        public static Statistic getRandom() {
            return statisticList.get(RAND.nextInt(statisticList.size()));
        }
    }

    /**
     * Clase de ayuda para conjuntos.
     */
    public static final class CONJUNTOS {

        /**
         * Regresa una lista de conjuntos.
         *
         * @return Un {@link Conjuntos}
         */
        public static List<Conjuntos> getList() {
            return conjuntosList;
        }

        /**
         * Regresa una lista de conjuntos.
         *
         * @param size El tamaño de la lista
         * @return Una lista de {@link Conjuntos}
         */
        public static List<Conjuntos> getRandomList(int size) {
            List<Conjuntos> list = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                list.add(conjuntosList.get(i));
            }
            Collections.shuffle(list);
            return list;
        }

        /**
         * Regresa un conjunto.
         *
         * @return Un {@link Conjuntos}
         */
        public static Conjuntos getRandom() {
            return conjuntosList.get(RAND.nextInt(conjuntosList.size()));
        }
    }

    /**
     * Clase de ayuda para matrices
     */
    public static final class MATRIX {

        /**
         * Regresa una lista de matrices.
         *
         * @return Una {@link Matrix}
         */
        public static List<Matrix> getList() {
            return matrixList;
        }

        /**
         * Regresa una lista de matrices.
         *
         * @param size El tamaño de la lista
         * @return Una lista de {@link Matrix}
         */
        public static List<Matrix> getRandomList(int size) {
            List<Matrix> list = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                list.add(matrixList.get(i));
            }
            Collections.shuffle(list);
            return list;
        }

        /**
         * Regresa una matrix.
         *
         * @return Una {@link Matrix}
         */
        public static Matrix getRandom() {
            return matrixList.get(RAND.nextInt(matrixList.size()));
        }
    }

    /**
     * Enum usado por recursos de la aplicación.
     * <p>
     * Añade el nombre de tu juego en el enum
     * para permitir a los recursos de la aplicación
     * saber cuál es tú juego.
     * </p>
     */
    public enum Game {
        EQUATION,
        FUNCTION,
        STATISTIC,
        CONJUNTOS,
        MATRIX;

        public String getResource() {
            return name().toLowerCase();
        }
    }
}
