package com.codejam.amadhea.game.registry.loader;

import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

/**
 * This file was created by Snack on 21/02/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

/**
 * Clase que carga un json a partir de una cadena de texto.
 *
 * @param <T> El tipo de objeto al que será traducido el JSON
 */
@SuppressWarnings({"unused","WeakerAccess"})
public class JsonLoader<T> {

    private final String LOGTAG = JsonLoader.class.getSimpleName();

    private TypeToken<T> responseType;
    private String jsonString;

    /**
     * Constructor del {@link JsonLoader}.
     *
     * @param jsonString El JSON en cadena de caracteres
     */
    public JsonLoader(String jsonString) {
        this.jsonString = jsonString;
    }

    /**
     * Constructor del {@link JsonLoader}.
     * <p>
     * Lee archivos .json y los convierte en listas, según el {@link TypeToken}
     * </p>
     *
     * @param responseType El {@link TypeToken}
     * @param jsonString   El JSON en cadena de caracteres
     */
    public JsonLoader(TypeToken<T> responseType, String jsonString) {
        this.responseType = responseType;
        this.jsonString = jsonString;
    }

    /**
     * Crea una lista según el {@link TypeToken} espeficidado.
     *
     * @return Una lista
     */
    public T construct() {
        return new Gson().fromJson(jsonString, responseType.getType());
    }

    /**
     * Retorna el {@link JSONArray} sin construir a partir de la cadena de caracteres.
     * <p>
     * Este método es para aquellos que quieran modificar
     * directamente el JSON sin nesecidad de convertirlo
     * a su lista de objetos previamente.
     * </p>
     *
     * @return El array de JSONs
     */
    @Nullable
    public JSONArray getJsonArray() {
        try {
            return new JSONArray(jsonString);
        }
        catch (Exception e) {
            Log.e(LOGTAG, "Unhandled exception while using JsonLoader", e);
        }
        return null;
    }
}
