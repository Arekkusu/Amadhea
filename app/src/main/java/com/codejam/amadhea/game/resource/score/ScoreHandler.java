package com.codejam.amadhea.game.resource.score;

import android.content.Context;
import android.content.SharedPreferences;

import com.codejam.amadhea.game.registry.GameRegistry.Game;
import com.codejam.amadhea.game.resource.GameProfile;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * This file was created by Snack on 23/02/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

@SuppressWarnings({"deprecation, unused", "WeakerAccess"})
public final class ScoreHandler {

    @Deprecated //Do not modify this
    private static final String AMADHEA_SCORES = "amadhea_scores";
    @Deprecated //Do not modify this
    private static final String SPLIT = "\\|";

    public static final String SCORE_SPLIT = "-"; //Usa este split si vas a almacenar más de 1 dato en tu score.
    public static final String DATE_FORM$HOURS_DATE = "HH:mm:ss - dd-MM-yyyy";
    public static final String DATE_FORM$HOURS = "HH:mm:ss";
    public static final String DATE_FORM$DATE = "dd-MM-yyyy";

    private ScoreHandler() {}

    /**
     * Retorna una lista ordenada de mayor a menor puntuaje de los Puntuajes de un juego registrados en la aplicación.
     *
     * @param context El {@link Context}, usado para acceder a los recursos de la aplicación
     * @param game    El tipo de juego {@link Game}
     * @return Una lista ordenada de {@link Score}
     */
    public static List<Score> getScores(Context context, Game game) {
        SharedPreferences preferences = context.getSharedPreferences(AMADHEA_SCORES, Context.MODE_PRIVATE);
        Set<String> stringSet = preferences.getStringSet(game.getResource(), new HashSet<String>());

        String[] scoreRaws = stringSet.toArray(new String[stringSet.size()]);
        List<Score> scoreList = new ArrayList<>();

        for (String scoreRaw : scoreRaws) {
            String[] score = scoreRaw.split(SPLIT, 3);
            scoreList.add(new Score(score[0], score[1], score[2]));
        }

        Collections.sort(scoreList);
        return scoreList;
    }

    /**
     * Método que convierte el mapa de Puntuajes a un Array de Strings ordenado por puntuación.
     *
     * @param context El {@link Context}, usado para acceder a los recursos de la aplicación
     * @param game    El tipo de juego {@link Game}
     * @return Un array ordenado de {@link String}
     */
    public static String[] getScoresText(Context context, Game game) {
        List<Score> list = getScores(context, game);
        String[] scores = new String[list.size()];

        for (int i = 0; i < list.size(); i++) {
            scores[i] = list.get(i).toString();
        }

        return scores;
    }

    /**
     * Método que convierte los datos del mejor Puntuaje a un Array de tamaño 3.
     *
     * @param context El {@link Context}, usado para acceder a los recursos de la aplicación
     * @param game    El tipo de juego {@link Game}
     * @return Un array de {@link String}
     */
    public static String[] getFirstScoreText(Context context, Game game) {
        List<Score> scores = ScoreHandler.getScores(context, game);

        String player = "-";
        String date = "-";
        String score = "-";
        if (!scores.isEmpty()) {
            Score data = scores.get(0);
            player = data.getPlayer();
            date = data.getDate();
            if (data.getScore().contains(SCORE_SPLIT)) {
                String[] split = data.getScore().split(SCORE_SPLIT);
                String splitScore = "";
                for (int i = 0; i < split.length; i++) {
                    String str = split[i];
                    if (i != split.length - 1) {
                        splitScore += str + " : ";
                    } else {
                        splitScore += str;
                    }
                }
                score = splitScore;
            } else {
                score = data.getScore();
            }
        }

        return new String[]{player, date, score};
    }

    /**
     * Añade un puntuaje al mapa de Puntuajes de un juego de la aplicación.
     *
     * @param context El {@link Context}, usado para acceder a los recursos de la aplicación
     * @param game    El tipo de juego {@link Game}
     * @param score   El puntuaje de tu {@link Score}
     */
    public static void addScore(Context context, Game game, String score) {
        addScore(context, game, getDateForm(DATE_FORM$DATE), score);
    }

    /**
     * Añade un puntuaje al mapa de Puntuajes de un juego de la aplicación.
     *
     * @param context    El {@link Context}, usado para acceder a los recursos de la aplicación
     * @param game       El tipo de juego {@link Game}
     * @param dateFormat El formato que la fecha va a tomar
     * @param score      El puntuaje de tu {@link Score}
     */
    public static void addScore(Context context, Game game, String dateFormat, String score) {
        addScore(context, game, getDateForm(dateFormat), score);
    }

    /**
     * Crea el formato de fecha.
     *
     * @param string El formato
     * @return Un nuevo formato de fecha
     */
    private static DateFormat getDateForm(String string) {
        return new SimpleDateFormat(string, Locale.getDefault());
    }

    /**
     * Añade un puntuaje al mapa de Puntuajes de un juego de la aplicación.
     *
     * @param context    El {@link Context}, usado para acceder a los recursos de la aplicación
     * @param game       El tipo de juego {@link Game}
     * @param dateFormat El {@link DateFormat} que la fecha va a tomar
     * @param score      El puntuaje de tu {@link Score}
     */
    public static void addScore(Context context, Game game, DateFormat dateFormat, String score) {
        if (!score.isEmpty()) {
            SharedPreferences preferences = context.getSharedPreferences(AMADHEA_SCORES, Context.MODE_PRIVATE);
            Set<String> stringSet = preferences.getStringSet(game.getResource(), new HashSet<String>());
            String player = GameProfile.isGuest() ? "Guest" : GameProfile.getProfile().getName();

            stringSet.add(player + "|" + dateFormat.format(new Date()) + "|" + score);

            SharedPreferences.Editor editor = preferences.edit();
            editor.remove(game.getResource());
            editor.apply();
            editor.putStringSet(game.getResource(), stringSet);
            editor.apply();
        }
    }

    /**
     * Borra todos los puntuajes del mapa de Puntuajes de un juego de la aplicación.
     *
     * @param context El {@link Context}, usado para acceder a los recursos de la aplicación
     * @param game    El tipo de juego {@link Game}
     */
    public static void resetScores(Context context, Game game) {
        SharedPreferences preferences = context.getSharedPreferences(AMADHEA_SCORES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(game.getResource());
        editor.apply();
    }
}
