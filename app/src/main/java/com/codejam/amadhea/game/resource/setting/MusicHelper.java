package com.codejam.amadhea.game.resource.setting;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * This file was created by Snack on 10/03/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

@SuppressWarnings("deprecation")
public final class MusicHelper {

    private static int backgroundMusic;
    private static SoundPool pool;
    private static int[] volumes;

    private MusicHelper() {}

    /**
     * Inicaliza el {@link SoundPool}
     *
     * @param context EL contexto de la aplicación
     */
    public static void initialize(Context context) {
        pool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        volumes = SettingsHandler.getVolumes(context);
    }

    /**
     * Carga un sonido a partir de un recurso y crea un {@link Sound}
     * <p>
     * No inicia el sonido, solo lo carga para su uso
     * </p>
     *
     * @param context  EL contexto de la aplicación
     * @param type     El tipo de sonido
     * @param raw      El recurso
     * @param priority La prioridad del sonido (Sobrepone los demás)
     * @param rate     La frecuencia o ruido del sonido
     * @return El sonido
     */
    public static Sound load(Context context, SoundType type, int raw, int priority, float rate) {
        int id = pool.load(context, raw, priority);
        return new Sound(type, id, priority, rate);
    }

    /**
     * Inicia un {@link Sound}
     *
     * @param sound  El sonido
     * @param repeat La cantidad de veces que debe repetirse, -1 para hacer un loop
     */
    public static void play(Sound sound, int repeat) {
        float volume = volumes[sound.getType().ordinal()];
        pool.play(sound.getId(), volume / 50, volume / 50, sound.getPriority(), repeat, sound.getRate());
    }

    /**
     * Ejecuta música de tipo {@link Service} usando la id de un recurso.
     *
     * @param context EL contexto de la aplicación
     * @param raw     Id del recurso
     * @return Un nuevo intent del servicio
     */
    public static synchronized Intent playPlayer(Context context, int raw) {
        backgroundMusic = raw;
        Intent intent = new Intent(context, SoundService.class);
        context.startService(intent);
        return intent;
    }

    /**
     * Detiene la ejecución música de tipo {@link Service}
     *
     * @param context El contexto de la aplicación
     * @param intent  El servicio que controla la música
     */
    public static synchronized void stopPlayer(Context context, Intent intent) {
        context.stopService(intent);
    }

    /**
     * Pausa un sonido de tipo {@link Sound}
     *
     * @param sound El sonido
     */
    public static void pause(Sound sound) {
        pool.pause(sound.getId());
    }

    /**
     * Resume un sonido de tipo {@link Sound}
     *
     * @param sound El sonido
     */
    public static void resume(Sound sound) {
        pool.resume(sound.getId());
    }

    /**
     * Detiene un sonido de tipo {@link Sound}
     *
     * @param sound El sonido
     */
    public static void stop(Sound sound) {
        pool.unload(sound.getId());
    }

    public static SoundPool getPool() {
        return pool;
    }

    public static int[] getVolumes() {
        return volumes;
    }

    public static void setVolumes(int[] volumes) {
        MusicHelper.volumes = volumes;
    }

    public enum SoundType {
        MUSIC,
        EFFECT
    }

    @SuppressWarnings("WeakerAccess")
    public static class Sound {

        private final SoundType type;
        private final int id;
        private final int priority;
        private final float rate;

        public Sound(SoundType type, int id, int priority, float rate) {
            this.type = type;
            this.id = id;
            this.priority = priority;
            this.rate = rate;
        }

        public SoundType getType() {
            return type;
        }

        public int getId() {
            return id;
        }

        public int getPriority() {
            return priority;
        }

        public float getRate() {
            return rate;
        }
    }

    public static class SoundService extends Service {

        public SoundService() {
            super();
        }

        private MediaPlayer mp;

        @Override
        public void onCreate() {
            super.onCreate();
            mp = MediaPlayer.create(this, backgroundMusic);
            mp.setVolume(volumes[0], volumes[0]);
            mp.setLooping(true);
        }

        @Nullable
        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }

        @Override
        public void onStart(Intent intent, int startId) {
            mp.start();
        }

        @Override
        public void onDestroy() {
            mp.stop();
            mp.release();
        }
    }
}
