package com.codejam.amadhea.game.resource.profile;

import java.util.UUID;

/**
 * This file was created by Snack on 20/02/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

@SuppressWarnings("unused")
public class Profile {

    private String uuid;
    private String name;

    /**
     * Contructor de un {@link Profile}.
     */
    public Profile(){}

    /**
     * Contructor de un {@link Profile}.
     * <p>
     *     Crea un nuevo perfil con un uuid único.
     * </p>
     *
     * @param name El nombre del perfil
     */
    public Profile(String name) {
        this.uuid = UUID.randomUUID().toString();
        this.name = name;
    }

    /**
     * Contructor de un {@link Profile}.
     * <p>
     *     Crea un perfil a partir de datos existentes.
     * </p>
     *
     * @param uuid El uuid del perfil
     * @param name El nombre del perfil
     */
    public Profile(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    @Deprecated
    public void setUuid(String uuid) { //No se debe modificar el uuid si el usuario ya está registrado
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
