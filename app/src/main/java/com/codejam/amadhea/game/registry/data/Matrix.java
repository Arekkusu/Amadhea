package com.codejam.amadhea.game.registry.data;

/**
 * This file was created by Snack on 04/04/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public class Matrix {

    private int[][] x;
    private String operation;
    private int[][] y;
    private int[][] answer;

    /**
     * Constructor de una matrix.
     *
     * @param x La matrix A
     * @param y La matrix B
     * @param operation La operación que se realiza
     * @param answer El resultado
     */
    public Matrix(int[][] x, int[][] y, String operation, int[][] answer) {
        this.x = x;
        this.y = y;
        this.operation = operation;
        this.answer = answer;
    }

    /**
     * Regresa la matrix A.
     *
     * @return  Un array de integers
     */
    public int[][] getX() {
        return x;
    }

    /**
     * Establece la matrix A.
     *
     * @param x Un array de integers
     */
    public void setX(int[][] x) {
        this.x = x;
    }

    /**
     * Regresa un {@link String} con la operación.
     *
     * @return La operación
     */
    public String getOperation() {
        return operation;
    }

    /**
     * Establece el tipo de operación.
     *
     * @param operation La operación
     */
    public void setOperation(String operation) {
        this.operation = operation;
    }

    /**
     * Regresa la matrix B.
     *
     * @return  Un array de integers
     */
    public int[][] getY() {
        return y;
    }

    /**
     * Establece la matrix B.
     *
     * @param y Un array de integers
     */
    public void setY(int[][] y) {
        this.y = y;
    }

    /**
     * Regresa un array {@code int[][]}.
     *
     * @return Un array de integers
     */
    public int[][] getAnswer() {
        return answer;
    }

    /**
     * Establece la respuesta de la matrix.
     *
     * @param answer Una array de integers
     */
    public void setAnswer(int[][] answer) {
        this.answer = answer;
    }

    /**
     * Revisa si los valores introducidos se asemejan a la respuesta.
     *
     * @param ans La matrix a comparar
     * @return Si las matrices coinciden
     */
    public boolean isEqual(int[][] ans) {
        boolean equals = true;

        if (ans.length == answer.length) {
            for (int i = 0; i < ans.length; i++) {
                int[] row = ans[i];
                if (row.length == answer[i].length) {
                    for (int j = 0; j < row.length; j++) {
                        int c = row[j];
                        if (c != answer[i][j]) {
                            equals = false;
                            break;
                        }
                    }
                } else {
                    equals = false;
                    break;
                }
            }
        } else {
            equals = false;
        }

        return equals;
    }
}
