package com.codejam.amadhea.game.registry.data;

import com.codejam.amadhea.core.TextHelper;

import java.util.List;

/**
 * This file was created by Snack on 26/01/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public class Equation implements IMathData<String, String[], Equation.Type> {

    private int type;
    private String string;
    private String[] answer;

    /**
     * Constructor de una ecuación.
     *
     * @param math    La ecuación
     * @param results Las posibles respuestas ó las repuestas x, y, z, etc
     */
    public Equation(String math, String[] results) {
        this.string = math;
        this.answer = results;
    }

    /**
     * Regresa el tipo de ecuación.
     *
     * @return El tipo de ecuación según el array de {@link Type}
     */
    public Type getType() {
        return Type.values()[type];
    }

    /**
     * Establece el tipo de ecuación.
     *
     * @param type Tipo de ecuación
     */
    @Deprecated
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Regresa la ecuación.
     *
     * @return Una ecuación
     */
    public String getString() {
        return string;
    }

    /**
     * Establece la ecuación.
     *
     * @param math La ecuación
     */
    @Deprecated
    public void setString(String math) {
        this.string = math;
    }

    /**
     * Regresa un {@link String[]} con las respuestas de la ecuación.
     *
     * @return Las respuestas de la ecuación
     */
    public String[] getAnswer() {
        return answer;
    }

    /**
     * Establece las respuestas de la ecuación.
     *
     * @param answer Las respuestas
     */
    @Deprecated
    public void setAnswer(String[] answer) {
        this.answer = answer;
    }

    /**
     * Revisa si los valores introducidos se asemejan a la respuesta
     *
     * @param strings Los valores
     * @param type    El tipo
     * @return Si la respuesta es correcta
     */
    @Override
    public boolean isCorrect(String[] strings, Type type) {
        List<String> toRemove = TextHelper.toList(answer);
        for (int i = 0; i < strings.length; i++) {
            if (toRemove.get(i).equalsIgnoreCase(strings[i])) {
                toRemove.remove(i);
            }
        }
        return toRemove.isEmpty();
    }

    /**
     * El tipo de ecuación, normalmente estas son Lineales, Cuadráticas, etc.
     */
    public enum Type {
        //Ecuación de primer grado
        PRIMER_GRADO,
        //Ecuación de segundo grado
        SEGUNDO_GRADO,
        //Ecuación con 2 incógnitas
        BI_INCOGNITA
    }
}
