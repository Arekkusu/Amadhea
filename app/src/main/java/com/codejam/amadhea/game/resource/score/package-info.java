@ParametersAreNonnullByDefault
@MethodsAreNonnullByDefault
package com.codejam.amadhea.game.resource.score;

import com.codejam.amadhea.core.intefaze.MethodsAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;