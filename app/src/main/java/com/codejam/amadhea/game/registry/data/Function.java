package com.codejam.amadhea.game.registry.data;

/**
 * This file was created by Snack on 01/02/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public class Function implements IMathData<String, String[][], Function.Type> {

    private int type;
    private String string;
    private String[][] answer;

    @Override
    public Type getType() {
        return Function.Type.values()[type];
    }

    @Override
    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String getString() {
        return string;
    }

    @Override
    public void setString(String string) {
        this.string = string;
    }

    @Override
    public String[][] getAnswer() {
        return answer;
    }

    @Override
    public void setAnswer(String[][] answer) {
        this.answer = answer;
    }

    /**
     * Revisa si los valores introducidos se asemejan a la respuesta
     *
     * @param strings Los valores
     * @param type    El tipo
     * @return Si la respuesta es correcta
     */
    @Override
    public boolean isCorrect(String[][] strings, Type type) {
        boolean equals = true;

        if (strings.length == answer.length) {
            for (int i = 0; i < strings.length; i++) {
                String[] row = strings[i];
                if (row.length == answer[i].length) {
                    for (int j = 0; j < row.length; j++) {
                        String c = row[j];
                        if (!c.equals(answer[i][j])) {
                            equals = false;
                            break;
                        }
                    }
                } else {
                    equals = false;
                    break;
                }
            }
        } else {
            equals = false;
        }

        return equals;
    }

    public enum Type {
        INYECTIVA,
        SOBREYECTIVA,
        BIYECTIVA
    }
}
