package com.codejam.amadhea.game.resource;

import com.codejam.amadhea.game.resource.profile.Profile;

/**
 * This file was created by Snack on 20/02/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

@SuppressWarnings("unused")
public final class GameProfile {

    private static Profile ACTIVE_PROFILE;
    private static boolean isGuest;

    private GameProfile() {}

    public static void setProfile(Profile profile) {
        ACTIVE_PROFILE = profile;
    }

    public static Profile getProfile() {
        return ACTIVE_PROFILE;
    }

    public static boolean isGuest() {
        return isGuest;
    }

    public static void setIsGuest(boolean bol) {
        isGuest = bol;
    }
}
