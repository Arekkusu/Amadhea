package com.codejam.amadhea.game.resource.score;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This file was created by Snack on 20/02/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

@SuppressWarnings({"unused","WeakerAccess"})
public class Score implements Comparable<Score> {

    private String player;
    private String date;
    private String score;

    public Score() {}

    public Score(String player, String date, String score) {
        this.player = player;
        this.date = date;
        this.score = score;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return player + ": " + date + " " + score;
    }

    @Override
    public int compareTo(Score o) {
        float x = toFloat(o.score);
        float y = toFloat(score);
        return x > y ? 1 : x < y ? -1 : 0;
    }

    private float toFloat(String score) {
        Matcher m = Pattern.compile("[^0-9]*([0-9]+).*").matcher(score);
        float sc = 0;
        if (m.matches()) {
            sc = Integer.parseInt(m.group(1));
        }
        return sc;
    }
}
