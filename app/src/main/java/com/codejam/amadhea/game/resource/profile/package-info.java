@ParametersAreNonnullByDefault
@MethodsAreNonnullByDefault
package com.codejam.amadhea.game.resource.profile;

import com.codejam.amadhea.core.intefaze.MethodsAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;