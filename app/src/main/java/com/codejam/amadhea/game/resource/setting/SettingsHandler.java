package com.codejam.amadhea.game.resource.setting;

import android.content.Context;
import android.content.SharedPreferences;

import com.codejam.amadhea.game.registry.GameRegistry;
import com.codejam.amadhea.game.resource.GameProfile;

/**
 * This file was created by Snack on 10/03/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

@SuppressWarnings("deprecation, unused")
public final class SettingsHandler {

    private SettingsHandler(){}

    @Deprecated
    private static final String AMADHEA_SETTINGS = "amadhea_settings";
    private static final String GAME_INFO = "game_info_$_";

    public static void setVolume(Context context, MusicHelper.SoundType soundType, int volume) {
        SharedPreferences preferences = context.getSharedPreferences(AMADHEA_SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(soundType.name(), volume);
        editor.apply();
    }

    public static int[] getVolumes(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(AMADHEA_SETTINGS, Context.MODE_PRIVATE);
        MusicHelper.SoundType[] soundTypes = MusicHelper.SoundType.values();
        int[] volumes = new int[soundTypes.length];
        for (int i = 0; i < soundTypes.length; i++) {
            volumes[i] = preferences.getInt(soundTypes[i].name(), 10);
        }
        return volumes;
    }

    public static void setShowGameInfo(Context context, GameRegistry.Game game, boolean show) {
        if(!GameProfile.isGuest()) {
            SharedPreferences preferences = context.getSharedPreferences(AMADHEA_SETTINGS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(GAME_INFO.replace("$", GameProfile.getProfile().getUuid()) + game.getResource(), show);
            editor.apply();
        }
    }

    public static boolean canShowGameInfo(Context context, GameRegistry.Game game) {
        SharedPreferences preferences = context.getSharedPreferences(AMADHEA_SETTINGS, Context.MODE_PRIVATE);
        return GameProfile.isGuest() || preferences.getBoolean(GAME_INFO.replace("$", GameProfile.getProfile().getUuid()) + game.getResource(), true);
    }
}
