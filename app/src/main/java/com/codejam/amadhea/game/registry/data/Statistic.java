package com.codejam.amadhea.game.registry.data;

/**
 * This file was created by Snack on 14/03/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public class Statistic {

    private String string;
    private String answer;
    private String[] fake;

    public Statistic(String string, String answer, String[] fake) {
        this.string = string;
        this.answer = answer;
        this.fake = fake;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String[] getFake() {
        return fake;
    }

    public void setFake(String[] fake) {
        this.fake = fake;
    }
}
