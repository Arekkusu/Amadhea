package com.codejam.amadhea.game.registry.loader;

import android.content.res.Resources;

import com.codejam.amadhea.R;
import com.codejam.amadhea.game.registry.GameRegistry;
import com.codejam.amadhea.game.registry.data.Conjuntos;
import com.codejam.amadhea.game.registry.data.Equation;
import com.codejam.amadhea.game.registry.data.Function;
import com.codejam.amadhea.game.registry.data.Matrix;
import com.codejam.amadhea.game.registry.data.Statistic;
import com.google.gson.reflect.TypeToken;

import java.util.List;

/**
 * This file was created by Snack on 26/01/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public final class AppResourceLoader {

    private AppResourceLoader() {}

    private static boolean LOADING_DONE = false;

    /**
     * Carga los recursos de la aplicación.
     *
     * @param resources Los {@link Resources}
     */
    public static synchronized void loadResources(Resources resources) {
        JsonLoader<List<Equation>> reader$equation = new JsonLoader<>(new TypeToken<List<Equation>>() {},
                new JsonReader().readJSON(resources, R.raw.ecuaciones));
        for (Equation equation : reader$equation.construct()) {
            GameRegistry.register(equation);
        }

        JsonLoader<List<Function>> reader$function = new JsonLoader<>(new TypeToken<List<Function>>() {},
                new JsonReader().readJSON(resources, R.raw.function));
        for (Function function : reader$function.construct()) {
            GameRegistry.register(function);
        }

        JsonLoader<List<Statistic>> reader$statistic = new JsonLoader<>(new TypeToken<List<Statistic>>() {},
                new JsonReader().readJSON(resources, R.raw.statistic));
        for (Statistic statistic : reader$statistic.construct()) {
            GameRegistry.register(statistic);
        }

        JsonLoader<List<Conjuntos>> reader$conjuntos = new JsonLoader<>(new TypeToken<List<Conjuntos>>() {},
                new JsonReader().readJSON(resources, R.raw.conjuntos));
        for (Conjuntos conjuntos : reader$conjuntos.construct()) {
            GameRegistry.register(conjuntos);
        }

        JsonLoader<List<Matrix>> reader$matrix = new JsonLoader<>(new TypeToken<List<Matrix>>() {},
                new JsonReader().readJSON(resources, R.raw.matrix));
        for (Matrix matrix : reader$matrix.construct()) {
            GameRegistry.register(matrix);
        }

        LOADING_DONE = true;
    }

    /**
     * Si los recursos de la aplicación han sido cargados, no significa que haya cargado sin errores.
     *
     * @return Ha terminado de cargar? True / False
     */
    public static boolean isLoadingDone() {
        return LOADING_DONE;
    }
}
