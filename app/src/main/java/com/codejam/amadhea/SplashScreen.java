package com.codejam.amadhea;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.codejam.amadhea.core.widget.DialogWrapper;
import com.codejam.amadhea.game.registry.loader.AppResourceLoader;
import com.codejam.amadhea.game.resource.GameProfile;
import com.codejam.amadhea.game.resource.profile.Profile;
import com.codejam.amadhea.game.resource.profile.ProfileHandler;
import com.codejam.amadhea.game.resource.setting.MusicHelper;
import com.codejam.amadhea.screen.GamesActivity;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codejam.amadhea.R.anim.abc_fade_out;
import static com.codejam.amadhea.R.anim.rotate_image;

/**
 * This file was created by Snack on 25/01/2017. It's distributed as part of MindFlex.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/MindFlex
 * MidFlex is open source, and is distributed under the MIT licence.
 */

public class SplashScreen extends Activity {

    private Intent music;
    private List<Profile> profileList;
    private Dialog dialog;
    private ListView list;
    private boolean login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final Animation animRotate = AnimationUtils.loadAnimation(getBaseContext(), rotate_image);
        final Animation animFadeOut = AnimationUtils.loadAnimation(getBaseContext(), abc_fade_out);
        final View imageProgress = findViewById(R.id._progress);
        final View imageLogo = findViewById(R.id._logo);

        animRotate.setDuration(3000);
        imageProgress.startAnimation(animRotate);

        animRotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                if (AppResourceLoader.isLoadingDone()) {
                    imageProgress.startAnimation(animFadeOut);
                    imageLogo.startAnimation(animFadeOut);
                    imageProgress.setVisibility(View.INVISIBLE);
                    imageLogo.setVisibility(View.INVISIBLE);
                    showProfiles();
                } else {
                    imageProgress.startAnimation(animRotate);
                    animRotate.setAnimationListener(this);
                }
            }
        });
        if(!AppResourceLoader.isLoadingDone()) {
            AppResourceLoader.loadResources(getResources());
        }
        MusicHelper.initialize(this);
        music = MusicHelper.playPlayer(this, R.raw.wander);
    }

    private void showProfiles() {
        ImageView imageView = (ImageView) findViewById(R.id._logo);
        imageView.setVisibility(View.VISIBLE);

        dialog = new DialogWrapper(this, R.layout.dialog_profiles)
                .setFeature(Window.FEATURE_NO_TITLE).build();
        dialog.show();

        dialog.findViewById(R.id.profile_dialog_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProfile();
            }
        });

        dialog.findViewById(R.id.profile_guest_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setGuest();
            }
        });

        list = (ListView) dialog.findViewById(R.id.profile_list);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setProfile(position);
            }
        });
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                removeProfile(position);
                return true;
            }
        });
        updateProfileList();
    }

    private void updateProfileList() {
        profileList = ProfileHandler.getProfiles(this);

        String[] names = new String[profileList.size()];
        for (int i = 0; i < names.length; i++) {
            names[i] = profileList.get(i).getName();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.view_list,
                R.id._list_text, names);
        list.setAdapter(adapter);
    }

    private void setProfile(int index) {
        if (!login) {
            GameProfile.setProfile(profileList.get(index));
            startGame();
        }
    }

    public void setGuest() {
        if (!login) {
            GameProfile.setIsGuest(true);
            startGame();
        }
    }

    private void startGame() {
        MusicHelper.stopPlayer(this, music);

        login = true;
        dialog.dismiss();
        Intent intent = new Intent(getBaseContext(), GamesActivity.class);
        startActivity(intent);
        finish();
    }

    public void addProfile() {
        TextView text = (TextView) dialog.findViewById(R.id.profile_dialog_text);
        if (!login) {
            String name = text.getText().toString();
            if (!name.isEmpty() && isText(name)) {
                ProfileHandler.addProfile(this, name);
                updateProfileList();
                text.setText("");
            } else {
                Toast.makeText(getApplicationContext(), getText(R.string.send_cant_add_profile), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isText(String a) {
        Pattern pat = Pattern.compile("^[0-9a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ_:().´&?!#$,\\-]" +
                "([0-9a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ_:().´&?!#$,\\-]| (?! ))+$");
        Matcher mat = pat.matcher(a);
        return mat.matches();
    }

    private void removeProfile(final int index) {
        new AlertDialog.Builder(this)
                .setTitle(getText(R.string.delete_profile_M))
                .setMessage(getText(R.string.do_delete_profile))
                .setPositiveButton(getText(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ProfileHandler.removeProfile(getApplicationContext(), profileList.get(index).getUuid());
                        updateProfileList();
                    }
                })
                .setNegativeButton(getText(R.string.no), null)
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(!login) {
            MusicHelper.getPool().release();
        }
    }
}
