package com.codejam.amadhea.screen;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.codejam.amadhea.R;

/**
 * This file was created by Snack on 03/03/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public class CreditsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);
    }

    public void goToUtez(View view) {
        goTo("http://www.utez.edu.mx/");
    }

    public void goToAmadhea(View view) {
        goTo("https://www.google.com/");
    }

    public void goToMIT(View view) {
        goTo("http://www.mit.edu/");
    }

    private void goTo(String web) {
        Uri uriUrl = Uri.parse(web);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    public void close(View view) {
        finish();
    }
}
