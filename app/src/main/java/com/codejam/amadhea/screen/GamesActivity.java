package com.codejam.amadhea.screen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.codejam.amadhea.R;
import com.codejam.amadhea.game.registry.GameRegistry;
import com.codejam.amadhea.game.resource.score.ScoreHandler;
import com.codejam.amadhea.game.resource.setting.MusicHelper;
import com.codejam.amadhea.screen.level.ConjuntosActivity;
import com.codejam.amadhea.screen.level.EquationActivity;
import com.codejam.amadhea.screen.level.FunctionActivity;
import com.codejam.amadhea.screen.level.MatrixActivity;
import com.codejam.amadhea.screen.level.StatisticActivity;

import static com.codejam.amadhea.R.anim.abc_fade_out;
import static com.codejam.amadhea.R.anim.rotate_image;

public class GamesActivity extends AppCompatActivity {

    private static Intent music;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private MusicHelper.Sound button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                showGame();
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        FloatingActionButton float_score = (FloatingActionButton) findViewById(R.id.icon_float_score);
        float_score.setOnClickListener(new View.OnClickListener() {

            private Snackbar scoreBar;
            private int oldState;

            @Override
            public void onClick(View view) {
                int newState = mViewPager.getCurrentItem();
                if (scoreBar == null || !scoreBar.isShown() || oldState != newState) {
                    scoreBar = Snackbar.make(view, "HighScore:\n" + getFirstScore(),
                            Snackbar.LENGTH_INDEFINITE).setAction("Action", null);
                    scoreBar.show();
                    oldState = newState;
                } else {
                    showGame();
                }
            }
        });
        showGame();
        setMusic(MusicHelper.playPlayer(this, R.raw.the_shard));
        button = MusicHelper.load(this, MusicHelper.SoundType.EFFECT, R.raw.button, 0, 1F);
    }

    public void openSettings(View view) {
        Intent intent = new Intent(getBaseContext(), SettingsActivity.class);
        startActivity(intent);
        MusicHelper.play(button, 0);
    }

    public void scroll(View view) {
        int scroll = view.getId() == R.id.arrow_right ? View.FOCUS_RIGHT : View.FOCUS_LEFT;
        mViewPager.arrowScroll(scroll);
        MusicHelper.play(button, 0);
    }

    public void closeApp(View view) {
        finish();
    }

    /**
     * Devuelve el texto del primer puntuaje más alto.
     *
     * @return El puntuaje más alto en texto
     */
    private String getFirstScore() {
        int currentItem = mViewPager.getCurrentItem();
        String[] score = ScoreHandler.getFirstScoreText(this, GameRegistry.Game.values()[currentItem]);

        return getText(R.string.player_) + " " + score[0] + "     " + getText(R.string.date_) + " "
                + score[1] + "     " + getText(R.string.score_) + " " + score[2];
    }

    /**
     * Crea un {@link Snackbar} que muestra en qué juego se encuentra en ese momento.
     */
    private void showGame() {
        Snackbar.make(findViewById(R.id.icon_float_credits),
                mSectionsPagerAdapter.getPageTitle(mViewPager.getCurrentItem()), Snackbar.LENGTH_LONG)
                .show();
    }

    /**
     * Abre el juego dependiendo de la posición del contenedor donde se encuentra.
     * <p>
     * Si quieres que tu contenedor tenga un botón y que este abra tu juego, agregalo aquí
     * </p>
     *
     * @param view El view que ejecuta el método
     */
    public void openGame(View view) {
        Class clazz = null;
        switch (mViewPager.getCurrentItem()) { //Si estoy en el contenedor 0 abre el juego de ecuaciones, 1 abre el juego de Funciones... etc
            case 0:
                clazz = EquationActivity.class;
                break;
            case 1:
                clazz = FunctionActivity.class;
                break;
            case 2:
                clazz = StatisticActivity.class;
                break;
            case 3:
                clazz = ConjuntosActivity.class;
                break;
            case 4:
                clazz = MatrixActivity.class;
        }
        if (clazz != null) {
            startActivity(new Intent(getBaseContext(), clazz));
        }
        MusicHelper.play(button, 0);
    }

    /**
     * Abre una nueva {@link android.app.Activity} y pasa la posición del contenedor
     * donde se encuentra
     *
     * @param view El view que ejecuta el método
     */
    public void openScore(View view) {
        Intent intent = new Intent(this, ScoreActivity.class);
        intent.putExtra("game", mViewPager.getCurrentItem());
        startActivity(intent);
        MusicHelper.play(button, 0);
    }

    public static Intent getMusic() {
        return music;
    }

    public static void setMusic(Intent music) {
        GamesActivity.music = music;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MusicHelper.stopPlayer(this, music);
        MusicHelper.getPool().release();
    }

    /**
     * Clase placeholder que devuelve una vista con un layout precargado.
     */
    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        /**
         * Regresa el layout de cada contenedor.
         * <p>
         * Si tienes un layout de tu juego y quieres que se muestre
         * en tu contenedor, debes añadirlo aquí
         * </p>
         *
         * @param inflater           El inflater
         * @param container          El contenedor
         * @param savedInstanceState El {@link Bundle}
         * @return El layout inflado dentro de un View
         */
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            int id;

            switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
                case 0:
                    id = R.layout.fragment_equation;
                    break;
                case 1:
                    id = R.layout.fragment_function;
                    break;
                case 2:
                    id = R.layout.fragment_statistic;
                    break;
                case 3:
                    id = R.layout.fragment_conjuntos;
                    break;
                case 4:
                    id = R.layout.fragment_matrix;
                    break;
                default:
                    id = R.layout.fragment_;
            }

            View view = inflater.inflate(id, container, false);
            View right = view.findViewById(R.id.arrow_right);
            View left = view.findViewById(R.id.arrow_left);
            Animation flick = AnimationUtils.loadAnimation(getContext(), abc_fade_out);
            flick.setRepeatCount(Animation.INFINITE);
            flick.setRepeatMode(Animation.REVERSE);
            flick.setDuration(1500);
            if(right != null) right.startAnimation(flick);
            if(left != null) left.startAnimation(flick);

            View game = view.findViewById(R.id._game);
            Animation rotate = AnimationUtils.loadAnimation(getContext(), rotate_image);
            rotate.setRepeatCount(Animation.INFINITE);
            rotate.setRepeatMode(Animation.RESTART);
            rotate.setDuration(100000);
            if(game != null) game.startAnimation(rotate);

            return view;
        }
    }

    /**
     * Clase que define cuántos contenedores se tienen y el nombre de cada uno.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position);
        }

        /**
         * El número máximo de contenedores.
         * <p>
         * Incrementa al número de juegos que quieras mostrar en el menú
         * </p>
         *
         * @return La cantidad de contenedores
         */
        @Override
        public int getCount() {
            return 5;
        }

        /**
         * Regresa un texto dependiendo del contenedor donde se encuentre.
         *
         * @param position La posición de tu contenedor
         * @return Un {@link CharSequence}
         */
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getText(R.string.app_name) + " - " + getString(R.string.highscore_info);
                case 1:
                    return getText(R.string.app_name) + " - " + getString(R.string.highscore_info);
                case 2:
                    return getText(R.string.app_name) + " - " + getString(R.string.highscore_info);
                case 3:
                    return getText(R.string.app_name) + " - " + getString(R.string.highscore_info);
                case 4:
                    return getText(R.string.app_name) + " - " + getString(R.string.highscore_info);
                default:
                    return "";
            }
        }
    }
}

