package com.codejam.amadhea.screen.level;

import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Chronometer;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codejam.amadhea.R;
import com.codejam.amadhea.game.registry.GameRegistry;
import com.codejam.amadhea.game.registry.data.Statistic;
import com.codejam.amadhea.game.resource.setting.MusicHelper;
import com.codejam.amadhea.game.resource.setting.SettingsHandler;
import com.codejam.amadhea.screen.GameInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.codejam.amadhea.game.registry.GameRegistry.RAND;
import static com.codejam.amadhea.game.resource.score.ScoreHandler.SCORE_SPLIT;

/**
 * This file was created by Snack on 14/03/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public class StatisticActivity extends AbstractScoreActivity {

    private CharSequence[] threats;
    private final int MAX_LEVELS = 10;

    private TextView[] textAnswers = new TextView[4];
    private List<Statistic> statistics;
    private String answer;
    private boolean canAnswer;
    private String time;
    private int multiCount;
    private int multiplier;
    private int points;
    private int level;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);
        showGameStartDialog();

        threats = new CharSequence[]{
                getText(R.string.shout_good),
                getText(R.string.shout_correct),
                getText(R.string.shout_excelent),
                getText(R.string.shout_perfect)
        };

        for (int i = 0; i < textAnswers.length; i++) {
            int id = getResources().getIdentifier("_answer_" + i, "id", getPackageName());
            textAnswers[i] = (TextView) findViewById(id);
        }

        if (SettingsHandler.canShowGameInfo(this, getGame())) {
            new GameInfo(this, getGame()).showGameInfo();
        }
    }

    @Override
    void beginGame() {
        statistics = GameRegistry.STATISTIC.getRandomList(10);
        Chronometer chronometer = (Chronometer) findViewById(R.id._chronometer);
        initListener(chronometer);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
        //Inicia el nivel
        tryAdvance();
    }

    private void tryAdvance() {
        if (++level > MAX_LEVELS) {
            MusicHelper.play(level_pass, 0);
            String[] score = getScore().split(SCORE_SPLIT);
            showGameOverDialog(getText(R.string.score_) + score[0], getText(R.string.time_) + score[1]);
        } else {
            nextLevel();
        }
    }

    private void nextLevel() {
        Statistic statistic = statistics.get(level - 1);
        ((TextView) findViewById(R.id._statistic_numbers)).setText(statistic.getString());
        answer = statistic.getAnswer();

        List<String> list = new ArrayList<>(textAnswers.length);
        list.add(answer);
        Collections.addAll(list, statistic.getFake());
        Collections.shuffle(list);

        for (int i = 0; i < textAnswers.length; i++) {
            TextView view = textAnswers[i];
            view.setTextColor(Color.BLACK);
            view.setEnabled(true);
            view.setText(list.get(i));
        }

        ((ProgressBar) findViewById(R.id._progress_bar)).setProgress(level);
        canAnswer = true;
        multiplier = 10;
    }

    public void checkAnswer(View view) {
        if (canAnswer) {
            for (TextView textView : textAnswers) {
                if (textView != view) {
                    textView.setTextColor(Color.GRAY);
                }
            }

            if (((TextView) view).getText().toString().equals(answer)) {
                points += multiplier + (points >> 2);
                ((TextView) findViewById(R.id._points)).setText(String.valueOf(points));
                destroyText(view, threats[RAND.nextInt(threats.length)]);
                MusicHelper.play(correct, 0);
            } else {
                ((TextView) view).setTextColor(Color.RED);
                MusicHelper.play(incorrect, 0);
                showCorrectText(answer);
            }

            canAnswer = false;
        }
    }

    private void showCorrectText(String answr) {
        for (TextView textView : textAnswers) {
            if (textView.getText().toString().equals(answr)) {
                textView.setEnabled(true);
                textView.setTextColor(Color.BLACK);
                destroyText(textView, "¡INCORRECTO!");
                break;
            }
        }
    }

    private void destroyText(View textView, CharSequence text) {
        Animation vaporise = AnimationUtils.loadAnimation(getBaseContext(), R.anim.vaporise_text);
        textView.startAnimation(vaporise);
        final Toast toast = makeToast(text);

        vaporise.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                toast.show();
                pauseChronometer();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!shouldSave()) {
                    resumeChronometer();
                }
                toast.cancel();
                tryAdvance();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public GameRegistry.Game getGame() {
        return GameRegistry.Game.STATISTIC;
    }

    @Override
    public String getScore() {
        return points + SCORE_SPLIT + time + " min";
    }

    @Override
    public boolean shouldSave() {
        return level > MAX_LEVELS;
    }

    @Override
    public void onTick(Chronometer chronometer) {
        time = chronometer.getText().toString();
        if (++multiCount == 2 && multiplier > 1) {
            --multiplier;
            multiCount = 0;
        }
    }
}

