package com.codejam.amadhea.screen.level;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codejam.amadhea.R;
import com.codejam.amadhea.core.intefaze.IDropListener;
import com.codejam.amadhea.core.math.MathHelper;
import com.codejam.amadhea.core.widget.DropListener;
import com.codejam.amadhea.core.widget.LongDragListener;
import com.codejam.amadhea.game.registry.GameRegistry;
import com.codejam.amadhea.game.registry.data.Equation;
import com.codejam.amadhea.game.resource.setting.MusicHelper;
import com.codejam.amadhea.game.resource.setting.SettingsHandler;
import com.codejam.amadhea.screen.GameInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.codejam.amadhea.R.anim.abc_fade_out;
import static com.codejam.amadhea.game.registry.GameRegistry.RAND;
import static com.codejam.amadhea.game.resource.score.ScoreHandler.SCORE_SPLIT;

/**
 * This file was created by Snack on 28/02/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public class EquationActivity extends AbstractScoreActivity implements IDropListener<TextView> {

    private final LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    private final Map<String, String> answerMap = new HashMap<>();
    private final Button[] buttons = new Button[5];
    private final int MAX_LEVELS = 10;

    private List<Equation> equations;
    private LinearLayout layoutAnswers;
    private TextView[] unknownVariables;
    private TextView[] textAnswers;
    private CharSequence[] threats;
    private Animation fade;
    private int variables;
    private int answerCount;
    private boolean canAnswer;
    private String time;
    private int multiCount;
    private int multiplier;
    private int points;
    private int level;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equation);

        fade = AnimationUtils.loadAnimation(this, abc_fade_out);
        fade.setRepeatCount(Animation.INFINITE);
        fade.setRepeatMode(Animation.REVERSE);
        fade.setDuration(1000);

        showGameStartDialog();
        layoutAnswers = (LinearLayout) findViewById(R.id._equation_layout);

        threats = new CharSequence[]{
                getText(R.string.shout_good),
                getText(R.string.shout_correct),
                getText(R.string.shout_excelent),
                getText(R.string.shout_perfect)
        };

        for (int i = 0; i < buttons.length; i++) {
            int id = getResources().getIdentifier("_button_" + i, "id", getPackageName());
            Button view = (Button) findViewById(id);
            view.setOnLongClickListener(new LongDragListener());
            buttons[i] = view;
        }

        if (SettingsHandler.canShowGameInfo(this, getGame())) {
            new GameInfo(this, getGame()).showGameInfo();
        }
    }

    @Override
    protected void beginGame() {
        equations = GameRegistry.EQUATION.getRandomList(MAX_LEVELS);
        Chronometer chronometer = (Chronometer) findViewById(R.id._chronometer);
        initListener(chronometer);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
        //Inicia el nivel
        tryAdvance();
    }

    private void tryAdvance() {
        if (++level > MAX_LEVELS) {
            MusicHelper.play(level_pass, 0);
            String[] score = getScore().split(SCORE_SPLIT);
            showGameOverDialog(getText(R.string.score_) + score[0], getText(R.string.time_) + score[1]);
        } else {
            nextLevel();
        }
    }

    private void nextLevel() {
        Equation equation = equations.get(level - 1);
        String eq = equation.getString();

        List<TextView> ans = new ArrayList<>();
        List<TextView> tAns = new ArrayList<>();
        layoutAnswers.removeAllViewsInLayout();
        answerMap.clear();

        String[] answers = equation.getAnswer();
        variables = 0;

        String line = "";
        for (int i = 0; i < eq.length(); i++) {
            String c = Character.toString(eq.charAt(i));
            if (Character.isLetter(eq.charAt(i))) {
                if (!line.isEmpty()) {
                    tAns.add(addEquationText(line));
                    line = "";
                }
                if (i + 1 != eq.length() && (eq.charAt(i + 1) == '²' || eq.charAt(i + 1) == '³')) {
                    c += eq.charAt(++i);
                }
                if(!answerMap.containsKey(c)) {
                    answerMap.put(c, answers[variables]);
                    if(variables + 1 < answers.length) {
                        ++variables;
                    }
                }
                ans.add(addDropText(c));
            } else {
                line += c;
            }
        }
        if (!line.isEmpty()) {
            tAns.add(addEquationText(line));
        }
        unknownVariables = ans.toArray(new TextView[ans.size()]);
        textAnswers = tAns.toArray(new TextView[ans.size()]);
        updateEquationSize();

        List<String> list = new ArrayList<>(buttons.length);
        Collections.addAll(list, answers);

        for (int i = answers.length; i < buttons.length; i++) {
            if (RAND.nextInt(6) != 0) {
                list.add(String.valueOf(RAND.nextInt(27)));
            } else {
                list.add((RAND.nextInt(10) == 0 ? "-" : "") +
                        RAND.nextInt(16) + "/" + RAND.nextInt(17));
            }
        }

        Collections.shuffle(list);

        for (int i = 0; i < buttons.length; i++) {
            Button view = buttons[i];
            view.setText(list.get(i));
            view.setVisibility(View.VISIBLE);
            view.setEnabled(true);
            int r = MathHelper.randomBetween(RAND, 80, 256);
            int g = MathHelper.randomBetween(RAND, 80, 256);
            int b = MathHelper.randomBetween(RAND, 80, 256);
            view.setBackgroundColor(Color.argb(255, r, g, b));
        }

        ((ProgressBar) findViewById(R.id._progress_bar)).setProgress(level);
        canAnswer = true;
        answerCount = 0;
        multiplier = 10;
    }

    private TextView addEquationText(String line) {
        TextView group = new TextView(this);
        group.setGravity(Gravity.CENTER);
        group.setText(line);
        layoutAnswers.addView(group, params);

        return group;
    }

    private TextView addDropText(String line) {
        TextView single = new TextView(this);
        single.setOnDragListener(new DropListener(this));
        single.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        single.setTextColor(Color.DKGRAY);
        single.setGravity(Gravity.CENTER);
        single.setText(line);
        single.startAnimation(fade);
        layoutAnswers.addView(single, params);

        return single;
    }

    private void updateEquationSize() {
        float size = 0;
        for (TextView view : textAnswers) {
            size += view.getText().length();
        }
        for (TextView view : unknownVariables) {
            size += view.getText().length();
        }

        size = 60 - size * 0.65F;
        for (TextView view : textAnswers) {
            view.setTextSize(size);
        }
        for (TextView view : unknownVariables) {
            view.setTextSize(size);
        }
    }

    public void checkAnswer(String key, TextView view) {
        if (canAnswer) {
            String fake = view.getText().toString();
            if (answerMap.get(key).equals(fake)) {
                points += multiplier + (points >> 2);
                ((TextView) findViewById(R.id._points)).setText(String.valueOf(points));
                destroyView(view, threats[RAND.nextInt(threats.length)]);
                MusicHelper.play(correct, 0);
                return;
            }
            MusicHelper.play(incorrect, 0);
            for (String s : answerMap.values()) {
                showCorrectAnswer(s);
            }
        }
    }

    private void showCorrectAnswer(String answr) {
        for (Button button : buttons) {
            if (button.isShown() && button.getText().toString().equals(answr)) {
                destroyView(button, getText(R.string.incorrect));
                break;
            }
        }
    }

    private void destroyView(final View view, CharSequence text) {
        Animation vaporise = AnimationUtils.loadAnimation(getBaseContext(), R.anim.vaporise_out);
        view.startAnimation(vaporise);
        final Toast toast = makeToast(text);

        vaporise.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                toast.show();
                pauseChronometer();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!shouldSave()) {
                    resumeChronometer();
                }
                view.setVisibility(View.INVISIBLE);
                toast.cancel();
                if (answerCount++ == variables) {
                    canAnswer = false;
                    tryAdvance();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public GameRegistry.Game getGame() {
        return GameRegistry.Game.EQUATION;
    }

    @Override
    public String getScore() {
        return points + SCORE_SPLIT + time + " min";
    }

    @Override
    public boolean shouldSave() {
        return level > MAX_LEVELS;
    }

    @Override
    public void onTick(Chronometer chronometer) {
        time = chronometer.getText().toString();
        if (++multiCount == 2 && multiplier > 1) {
            --multiplier;
            multiCount = 0;
        }
    }

    @Override
    public void onDrop(TextView target, TextView drop) {
        CharSequence text = target.getText();
        for (TextView textView : unknownVariables) {
            if (text.equals(textView.getText()) || variables == 0) {
                if(textView.getText().toString().contains("²")) {
                    textView.setText("(" + drop.getText() + ")" + "²");
                } else {
                    textView.setText("(" + drop.getText() + ")");
                }
                textView.setTypeface(null, Typeface.ITALIC);
                textView.setTag(drop.getId());
                textView.setPaintFlags(0);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                textView.cancelDragAndDrop();
            }
            textView.clearAnimation();
        }
        updateEquationSize();
        checkAnswer(text.toString(), drop);
    }
}
