package com.codejam.amadhea.screen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.SeekBar;

import com.codejam.amadhea.R;
import com.codejam.amadhea.game.resource.setting.MusicHelper;
import com.codejam.amadhea.game.resource.setting.SettingsHandler;

/**
 * This file was created by Snack on 10/03/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        SeekBar music = (SeekBar) findViewById(R.id.volume_music);
        SeekBar effect = (SeekBar) findViewById(R.id.volume_effect);

        music.setOnSeekBarChangeListener(new MusicChangeListener(MusicHelper.SoundType.MUSIC) {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                super.onStopTrackingTouch(seekBar);

                MusicHelper.stopPlayer(seekBar.getContext(), GamesActivity.getMusic());
                GamesActivity.setMusic(MusicHelper.playPlayer(seekBar.getContext(), R.raw.the_shard));
            }
        });
        effect.setOnSeekBarChangeListener(new MusicChangeListener(MusicHelper.SoundType.EFFECT));

        int[] vol = SettingsHandler.getVolumes(this);
        music.setProgress(vol[0]);
        effect.setProgress(vol[1]);
    }

    public void closeSettings(View view) {
        finish();
    }

    public void openCredits(View view) {
        Intent intent = new Intent(getBaseContext(), CreditsActivity.class);
        startActivity(intent);
    }

    private class MusicChangeListener implements SeekBar.OnSeekBarChangeListener {

        private final MusicHelper.SoundType type;

        MusicChangeListener(MusicHelper.SoundType type) {
            this.type = type;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            SettingsHandler.setVolume(seekBar.getContext(), type, seekBar.getProgress());
            MusicHelper.setVolumes(SettingsHandler.getVolumes(seekBar.getContext()));
        }
    }
}
