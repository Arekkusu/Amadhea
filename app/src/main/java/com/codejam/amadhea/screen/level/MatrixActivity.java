package com.codejam.amadhea.screen.level;

import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Chronometer;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codejam.amadhea.R;
import com.codejam.amadhea.core.intefaze.IDropListener;
import com.codejam.amadhea.core.math.MathHelper;
import com.codejam.amadhea.core.widget.DropListener;
import com.codejam.amadhea.core.widget.LongDragListener;
import com.codejam.amadhea.core.widget.TextMatrix;
import com.codejam.amadhea.game.registry.GameRegistry;
import com.codejam.amadhea.game.registry.data.Matrix;
import com.codejam.amadhea.game.resource.setting.MusicHelper;
import com.codejam.amadhea.game.resource.setting.SettingsHandler;
import com.codejam.amadhea.screen.GameInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.codejam.amadhea.R.anim.abc_fade_out;
import static com.codejam.amadhea.game.registry.GameRegistry.RAND;
import static com.codejam.amadhea.game.resource.score.ScoreHandler.SCORE_SPLIT;

/**
 * This file was created by Snack on 04/04/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public class MatrixActivity extends AbstractScoreActivity implements IDropListener<TextMatrix> {

    private TextMatrix[] questionTexts = new TextMatrix[2];
    private final TextMatrix[] answers = new TextMatrix[5];
    private final int MAX_LEVELS = 10;

    private List<Matrix> matrixList;
    private TextView operation;
    private TextMatrix drop;
    private CharSequence[] threats;
    private Matrix matrix;
    private boolean canAnswer;
    private String time;
    private int multiCount;
    private int multiplier;
    private int points;
    private int level;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matrix);
        showGameStartDialog();

        threats = new CharSequence[]{
                getText(R.string.shout_good),
                getText(R.string.shout_correct),
                getText(R.string.shout_excelent),
                getText(R.string.shout_perfect)
        };

        for (int i = 0; i < answers.length; i++) {
            int id = getResources().getIdentifier("b_" + i, "id", getPackageName());
            TextMatrix view = (TextMatrix) findViewById(id);
            view.setOnLongClickListener(new LongDragListener());
            answers[i] = view;
        }

        for (int i = 0; i < questionTexts.length; i++) {
            int id = getResources().getIdentifier("ans_" + i, "id", getPackageName());
            TextMatrix view = (TextMatrix) findViewById(id);
            questionTexts[i] = view;
        }

        operation = (TextView) findViewById(R.id._operation);
        drop = (TextMatrix) findViewById(R.id.answer);
        drop.setOnDragListener(new DropListener(this));

        if(SettingsHandler.canShowGameInfo(this, getGame())) {
            new GameInfo(this, getGame()).showGameInfo();
        }
    }

    @Override
    void beginGame() {
        matrixList = GameRegistry.MATRIX.getRandomList(10);
        Chronometer chronometer = (Chronometer) findViewById(R.id._chronometer);
        initListener(chronometer);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
        //Inicia el nivel
        tryAdvance();
    }

    private void tryAdvance() {
        if (++level > MAX_LEVELS) {
            MusicHelper.play(level_pass, 0);
            String[] score = getScore().split(SCORE_SPLIT);
            showGameOverDialog(getText(R.string.score_) + score[0], getText(R.string.time_) + score[1]);
        } else {
            nextLevel();
        }
    }

    private void nextLevel() {
        matrix = matrixList.get(level - 1);

        questionTexts[0].setNeoMatrix(matrix.getX());
        questionTexts[1].setNeoMatrix(matrix.getY());
        operation.setText(matrix.getOperation());

        List<int[][]> list = new ArrayList<>(answers.length);
        list.add(matrix.getAnswer());

        for (int i = 1; i < answers.length; i++) {
            int[][] old = matrix.getAnswer();
            int[][] neo = new int[old.length][];

            for (int j = 0; j < neo.length; j++) {
                neo[j] = new int[old[j].length];
                for (int k = 0; k < neo[j].length; k++) {
                    if(RAND.nextBoolean()) {
                        neo[j][k] = RAND.nextInt(10);
                    } else {
                        neo[j][k] = old[j][k];
                    }
                }
            }

            list.add(neo);
        }

        Collections.shuffle(list);

        for (int i = 0; i < answers.length; i++) {
            TextMatrix m = answers[i];
            m.setNeoMatrix(list.get(i));

            int r = MathHelper.randomBetween(RAND, 80, 256);
            int g = MathHelper.randomBetween(RAND, 80, 256);
            int b = MathHelper.randomBetween(RAND, 80, 256);
            m.setBackgroundColor(Color.argb(255, r, g, b));
        }

        ((ProgressBar) findViewById(R.id._progress_bar)).setProgress(level);
        drop.setTag(null);
        drop.setText("-");
        canAnswer = true;
        multiplier = 10;
    }

    public void checkAnswer(TextMatrix neo) {
        if(matrix.isEqual(neo.getNeoMatrix())) {
            points += multiplier + (points >> 2);
            ((TextView) findViewById(R.id._points)).setText(String.valueOf(points));
            destroyText(neo, threats[RAND.nextInt(threats.length)]);
            MusicHelper.play(correct, 0);
        } else {
            MusicHelper.play(incorrect, 0);
            showCorrectText();
        }
    }

    private void showCorrectText() {
        for (TextMatrix m : answers) {
            if(matrix.isEqual(m.getNeoMatrix())) {
                destroyText(m, getText(R.string.incorrect));
                break;
            }
        }
    }

    private void destroyText(View view, CharSequence text) {
        Animation vaporise = AnimationUtils.loadAnimation(getBaseContext(), abc_fade_out);
        vaporise.setDuration(1500L);
        view.startAnimation(vaporise);
        final Toast toast = makeToast(text);

        vaporise.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                toast.show();
                pauseChronometer();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!shouldSave()) {
                    resumeChronometer();
                }
                toast.cancel();
                tryAdvance();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public GameRegistry.Game getGame() {
        return GameRegistry.Game.MATRIX;
    }

    @Override
    public String getScore() {
        return points + SCORE_SPLIT + time + " min";
    }

    @Override
    public boolean shouldSave() {
        return level > MAX_LEVELS;
    }

    @Override
    public void onTick(Chronometer chronometer) {
        time = chronometer.getText().toString();
        if (++multiCount == 2 && multiplier > 1) {
            --multiplier;
            multiCount = 0;
        }
    }

    @Override
    public void onDrop(TextMatrix target, TextMatrix drop) {
        if (canAnswer) {
            target.setNeoMatrix(drop.getNeoMatrix());
            checkAnswer(drop);
            canAnswer = false;
        }
    }
}
