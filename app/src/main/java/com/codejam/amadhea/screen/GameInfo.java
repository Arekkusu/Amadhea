package com.codejam.amadhea.screen;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.codejam.amadhea.R;
import com.codejam.amadhea.core.widget.DialogWrapper;
import com.codejam.amadhea.game.registry.GameRegistry;
import com.codejam.amadhea.game.resource.setting.SettingsHandler;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

/**
 * This file was created by Snack on 30/03/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public class GameInfo {

    private final List<Integer> drawables = new ArrayList<>();
    private final Activity context;
    private final GameRegistry.Game game;

    public GameInfo(Activity context, GameRegistry.Game game) {
        this.context = context;
        this.game = game;
    }

    public void showGameInfo() {
        Resources resources = context.getResources();
        int i = 0;

        int drawable = getNext(resources, i);
        while (drawable != 0) {
            drawables.add(drawable);
            drawable = getNext(resources, ++i);
        }

        final Dialog dialog = new DialogWrapper(context, R.layout.dialog_info)
                .setFeature(Window.FEATURE_NO_TITLE)
                .setCancelable(true).build();

        if(dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        dialog.findViewById(R.id._close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SettingsHandler.setShowGameInfo(context, game, false);
            }
        });

        dialog.findViewById(R.id._dismiss).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        AutoScrollViewPager viewPager = (AutoScrollViewPager) dialog.findViewById(R.id.container);
        SectionsPagerAdapter pagerAdapter = new SectionsPagerAdapter(context, drawables);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setInterval(3000L);
        viewPager.startAutoScroll();

        CirclePageIndicator pageIndicator = (CirclePageIndicator) dialog.findViewById(R.id.page_indicator);
        pageIndicator.setViewPager(viewPager);
        pageIndicator.setCurrentItem(0);

        dialog.show();
    }

    private int getNext(Resources resources, int pos) {
        return resources.getIdentifier(game.getResource() + "_" + pos, "drawable", context.getPackageName());
    }

    private class SectionsPagerAdapter extends PagerAdapter {

        private final List<Integer> drawables;
        private final LayoutInflater inflater;

        SectionsPagerAdapter(Activity context, List<Integer> drawables) {
            this.drawables = drawables;
            inflater = context.getLayoutInflater();
        }

        @Override
        public int getCount() {
            return drawables.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = inflater.inflate(R.layout.fragment_info, container, false);
            ((ImageView) view.findViewById(R.id._image)).setImageResource(drawables.get(position));
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {

        }
    }
}
