package com.codejam.amadhea.screen.level;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codejam.amadhea.R;
import com.codejam.amadhea.game.registry.GameRegistry;
import com.codejam.amadhea.game.registry.data.Conjuntos;
import com.codejam.amadhea.game.resource.setting.MusicHelper;
import com.codejam.amadhea.game.resource.setting.SettingsHandler;
import com.codejam.amadhea.screen.GameInfo;

import java.util.List;

import static com.codejam.amadhea.R.anim.abc_fade_out;
import static com.codejam.amadhea.game.registry.GameRegistry.RAND;
import static com.codejam.amadhea.game.resource.score.ScoreHandler.SCORE_SPLIT;

/**
 * This file was created by Alberto on 15/03/17. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public class ConjuntosActivity extends AbstractScoreActivity {

    private final Button[] buttons = new Button[2];
    private final int MAX_LEVELS = 10;

    private TextView affirmationText;
    private TextView answerText;
    private Conjuntos answer;
    private CharSequence[] threats;
    private List<Conjuntos> conjuntos;
    private boolean canAnswer;
    private String time;
    private int multiplier;
    private int points;
    private int level;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conjuntos);
        showGameStartDialog();

        threats = new CharSequence[]{
                getText(R.string.shout_good),
                getText(R.string.shout_correct),
                getText(R.string.shout_excelent),
                getText(R.string.shout_perfect)
        };

        affirmationText = (TextView) findViewById(R.id._affirmation);
        answerText = (TextView) findViewById(R.id._answer);

        for (int i = 0; i < buttons.length; i++) {
            int id = getResources().getIdentifier("_button_" + i, "id", getPackageName());
            buttons[i] = (Button) findViewById(id);
        }

        if (SettingsHandler.canShowGameInfo(this, getGame())) {
            new GameInfo(this, getGame()).showGameInfo();
        }
    }

    @Override
    void beginGame() {
        conjuntos = GameRegistry.CONJUNTOS.getRandomList(10);
        Chronometer chronometer = (Chronometer) findViewById(R.id._chronometer);
        initListener(chronometer);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
        //Inicia el nivel
        tryAdvance();
    }

    private void tryAdvance() {
        if (++level > MAX_LEVELS) {
            MusicHelper.play(level_pass, 0);
            String[] score = getScore().split(SCORE_SPLIT);
            showGameOverDialog(getText(R.string.score_) + score[0], getText(R.string.time_) + score[1]);
        } else {
            nextLevel();
        }
    }

    private void nextLevel() {
        answer = conjuntos.get(level - 1);
        affirmationText.setText(answer.getString());

        answerText.setText(answer.getFake());
        answerText.setTypeface(null, 0);
            answerText.setBackgroundColor(Color.WHITE);

        for (Button view : buttons) {
            view.setTextColor(Color.BLACK);
            view.setEnabled(true);
        }

        ((ProgressBar) findViewById(R.id._progress_bar)).setProgress(level);
        canAnswer = true;
        multiplier = 10;
    }

    public void isTrue(View view) {
        if(canAnswer) {
            checkAnswer(view, answer.is());
        }
    }

    public void isFalse(View view) {
        if(canAnswer) {
            checkAnswer(view, !answer.is());
        }
    }

    public void checkAnswer(View view, boolean equals) {
        if(equals) {
            points += multiplier + (points >> 2);
            ((TextView) findViewById(R.id._points)).setText(String.valueOf(points));
            destroyText(view, threats[RAND.nextInt(threats.length)]);
            MusicHelper.play(correct, 0);
        } else {
            ((TextView) view).setTextColor(0xCC0000);
            MusicHelper.play(incorrect, 0);
            showCorrectText();
        }
        answerText.setTypeface(null, Typeface.ITALIC);
        view.setEnabled(false);
        canAnswer = false;
    }

    private void showCorrectText() {
        Button ans = answer.is() ? buttons[0] : buttons[1];
        ans.setTextColor(Color.BLACK);

        if(!answerText.getText().toString().equals(answer.getAnswer())) {
            answerText.setText(answer.getAnswer());
            answerText.setTypeface(null, Typeface.BOLD_ITALIC);
            answerText.setBackgroundColor(Color.RED);
        }

        destroyText(ans, "¡INCORRECTO!");
    }

    private void destroyText(View textView, CharSequence text) {
        Animation vaporise = AnimationUtils.loadAnimation(getBaseContext(), abc_fade_out);
        vaporise.setDuration(2500);
        textView.startAnimation(vaporise);
        final Toast toast = makeToast(text, Gravity.CENTER, 0, 100);

        vaporise.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                toast.show();
                pauseChronometer();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!shouldSave()) {
                    resumeChronometer();
                }
                toast.cancel();
                tryAdvance();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public GameRegistry.Game getGame() {
        return GameRegistry.Game.CONJUNTOS;
    }

    @Override
    public String getScore() {
        return points + SCORE_SPLIT + time + " min";
    }

    @Override
    public boolean shouldSave() {
        return level > MAX_LEVELS;
    }

    @Override
    public void onTick(Chronometer chronometer) {
        time = chronometer.getText().toString();
        if (multiplier > 1) {
            --multiplier;
        }
    }
}
