package com.codejam.amadhea.screen;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.codejam.amadhea.R;
import com.codejam.amadhea.core.widget.ColumnListAdapter;
import com.codejam.amadhea.game.registry.GameRegistry;
import com.codejam.amadhea.game.resource.score.Score;
import com.codejam.amadhea.game.resource.score.ScoreHandler;

import java.util.List;

import static com.codejam.amadhea.game.resource.score.ScoreHandler.SCORE_SPLIT;

/**
 * This file was created by Snack on 23/02/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public class ScoreActivity extends AppCompatActivity {

	private final GameRegistry.Game[] games = GameRegistry.Game.values();
    private List<Score> scores;
    private int game;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        game = getIntent().getIntExtra("game", 0);
        ((ListView) findViewById(R.id.S_score_list)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showDetail(position);
            }
        });
        updateScores();
    }

    private void showDetail(int position) {
        if(position == 0) return;
        Score score = scores.get(position - 1);
        new AlertDialog.Builder(this)
                .setTitle(getText(R.string.highscores))
                .setMessage(getText(R.string.player_) + "\n- " + score.getPlayer() + "\n" +
                        getScoreLazy(score) +
                        getText(R.string.date_) + "\n- " + score.getDate())
                .show();
    }

    private String getScoreLazy(Score data) {
        String score;

        if (data.getScore().contains(SCORE_SPLIT)) {
            String[] split = data.getScore().split(SCORE_SPLIT);
            String extra = "";
            for (String s : split) {
                extra += "\n- " + s;
            }
            score = getText(R.string.score_) + extra + "\n";
        } else {
            score = getText(R.string.score_) + data.getScore();
        }

        return score;
    }

	public void deleteScores(View view) {
        new AlertDialog.Builder(this)
                .setTitle(getText(R.string.delete_highscore_M))
                .setMessage(getText(R.string.do_delete_highscores))
                .setPositiveButton(getText(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ScoreHandler.resetScores(getApplicationContext(),games[game]);
                        Toast.makeText(getApplicationContext(), getText(R.string.send_delete_highscores), Toast.LENGTH_SHORT).show();
                        updateScores();
                    }
                })
                .setNegativeButton(getText(R.string.no), null).show();
	}

	private void updateScores() {
        scores = ScoreHandler.getScores(this, games[game]);

        ColumnListAdapter.Builder builder = new ColumnListAdapter.Builder(this);
        builder.addRow()
                .addColumn(getView(getText(R.string.player)))
                .addColumn(getView(getText(R.string.date)))
                .addColumn(getView(getText(R.string.score)))
                .bake();
        for (int i = 0; i < scores.size(); i++) {
            Score score = scores.get(i);
            builder.addRow()
                    .addColumn(getView(score.getPlayer()))
                    .addColumn(getView(score.getDate()))
                    .addColumn(getView(score.getScore().contains(SCORE_SPLIT) ?
                                    score.getScore().split(SCORE_SPLIT)[0] :
                                    score.getScore())
                    ).bake();
        }

		((ListView) findViewById(R.id.S_score_list)).setAdapter(builder.make());
	}

    private View getView(CharSequence string) {
        TextView view = new TextView(this);
        view.setText(string);
        view.setGravity(Gravity.CENTER);
        view.setTextSize(25);
        view.setMaxLines(1);
        return view;
    }
}
