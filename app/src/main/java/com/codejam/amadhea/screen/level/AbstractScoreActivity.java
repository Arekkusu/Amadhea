package com.codejam.amadhea.screen.level;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.widget.Chronometer;
import android.widget.Toast;

import com.codejam.amadhea.R;
import com.codejam.amadhea.core.intefaze.IScoreTickable;
import com.codejam.amadhea.game.resource.score.ScoreHandler;
import com.codejam.amadhea.game.resource.setting.MusicHelper;

import static com.codejam.amadhea.game.registry.GameRegistry.Game;

/**
 * This file was created by Snack on 27/02/2017. It's distributed as part of AMADHEA.
 * Get the source code in GitHub: https://github.com/ArekkuusuJerii/AMADHEA
 * AMADHEA is open source, and is distributed under the MIT licence.
 */

public abstract class AbstractScoreActivity extends AppCompatActivity implements IScoreTickable<Chronometer> {

    private AlertDialog.Builder gameStart;
    private AlertDialog.Builder gameOver;
    private AlertDialog.Builder pause;
    private Intent baseIntent;
    protected MusicHelper.Sound correct;
    protected MusicHelper.Sound incorrect;
    protected MusicHelper.Sound level_pass;
    private boolean saved; //Si este juego ya ha guardado su puntuaje
    private long timeWhenStopped = 0;
    public Chronometer chronometer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseIntent = getIntent();
        correct = MusicHelper.load(this, MusicHelper.SoundType.EFFECT, R.raw.correct, 0, 1F);
        incorrect = MusicHelper.load(this, MusicHelper.SoundType.EFFECT, R.raw.incorrect, 0, 1F);
        level_pass = MusicHelper.load(this, MusicHelper.SoundType.EFFECT, R.raw.level_pass, 0, 1.5F);
        init();
    }

    private void init() {
        gameStart = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.start))
                .setMessage(getText(R.string.do_enter_game))
                .setIcon(android.R.drawable.ic_media_play)
                .setPositiveButton(getText(R.string.begin), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int button) {
                        beginGame();
                    }
                })
                .setNegativeButton(getText(R.string.exit), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int button) {
                        finish();
                    }
                }).setCancelable(false);

        gameOver = new AlertDialog.Builder(this)
                .setTitle(getText(R.string.end_of_game))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(getText(R.string.exit), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int button) {
                        finish();
                    }
                })
                .setNegativeButton(getText(R.string.retry), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        startActivity(baseIntent);
                    }
                }).setCancelable(false);

        pause = new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_media_pause)
                .setTitle(getText(R.string.pause))
                .setMessage(getText(R.string.do_exit_game))
                .setPositiveButton(getText(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton(getText(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        resumeChronometer();
                    }
                }).setCancelable(false);
    }

    /**
     * Muestra el diálogo para inicar el juego.
     */
    protected void showGameStartDialog() {
        gameStart.show();
    }

    /**
     * Muestra el diálogo para acabar el juego
     */
    protected void showGameOverDialog(String... strings) {
        String message = "";
        for (String string : strings) {
            message += string + "\n";
        }
        gameOver.setMessage(message);
        gameOver.show();
    }

    /**
     * Método que previene que el usuario salga del nivel de
     * imprevisto dando la opción de salir o no salir.
     */
    @Override
    public void onBackPressed() {
        pauseChronometer();
        pause.show();
    }

    /**
     * Crea un {@link Toast} en una posición default "deseable".
     *
     * @param message El mensaje
     * @return El Toast creado
     */
    protected Toast makeToast(CharSequence message) {
        return makeToast(message, Gravity.CENTER, 0, 0);
    }

    /**
     * Crea un {@link Toast} en una posición y offset indicado.
     *
     * @param message El mensaje
     * @param x       Posición offset en horizontal
     * @param y       Posición offset en vertical
     * @return El Toast creado
     */
    @SuppressLint("ShowToast")
    protected Toast makeToast(CharSequence message, int gravity, int x, int y) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.setGravity(gravity, x, y);
        return toast;
    }

    /**
     * Inicia el juego, la implementación puede variar.
     */
    abstract void beginGame();

    /**
     * Crea un nuevo listener para el cronómetro.
     * <p>
     * En este caso, el listener ejecuta el método {@code conTick(T t)}
     * implementado de la interfaz {@link IScoreTickable}
     * </p>
     *
     * @param chronometer El cronómetro que se quiere controlar
     */
    @Override
    public void initListener(Chronometer chronometer) {
        this.chronometer = chronometer;
        chronometer.setOnChronometerTickListener(
                new Chronometer.OnChronometerTickListener() {
                    @Override
                    public void onChronometerTick(Chronometer chronometer) {
                        onTick(chronometer);
                    }
                }
        );
    }

    /**
     * Pausa el cronometro de la actividad.
     */
    public void pauseChronometer() {
        if (chronometer != null) {
            timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
            chronometer.stop();
        }
    }

    /**
     * Continua con el conteo del cronometro de la actividad.
     */
    public void resumeChronometer() {
        if (chronometer != null) {
            chronometer.setBase(SystemClock.elapsedRealtime() + timeWhenStopped);
            chronometer.start();
        }
    }

    /**
     * El juego al que tu activity va guardar su puntuaje.
     *
     * @return El juego
     */
    abstract public Game getGame();

    /**
     * El puntuaje de tu juego.
     *
     * @return EL puntuaje
     */
    abstract public String getScore();

    /**
     * Registra el puntuaje y marca este juego cono guardado.
     * <p>
     * En caso que se destruya la aplicación, el puntuaje
     * se registrara y marcará este juego como guardado.
     * </p>
     */
    @SuppressWarnings("ConstantConditions")
    protected void applyScore() {
        if (getGame() == null) throw new NullPointerException("Game cannot be null!");
        ScoreHandler.addScore(this, getGame(), getScore());
        saved = true;
    }

    /**
     * Método usado para determinar si el juego puede o no guardar el
     * puntuaje sin haber finalizado el juego
     *
     * @return Si el juego puede guardar su puntuaje
     */
    public boolean shouldSave() {
        return true;
    }

    /**
     * Método llamado cuando la aplicación o la actividad es destruida.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!saved && shouldSave()) {
            applyScore();
        }
    }
}